package com.awoo;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Mod(
        modid = "chatfilters",
        name = "Chatfilters",
        version = "0.0.3_2017-05-26"
)
public class Chatfilters {
    public static final String ID = "chatfilters";
    public static final String VERSION = "0.0.3_2017-05-26";
    public static final String NAME = "Chatfilters";
    public static Boolean DISABLED = false;
    public static Logger log = LogManager.getLogger("chatfilters");

    @EventHandler
    public void init(FMLInitializationEvent event) {
        FMLCommonHandler.instance().bus().register(new KeyHandler());
        MinecraftForge.EVENT_BUS.register(new ChatHandler());
    }
}
