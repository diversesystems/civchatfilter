package com.awoo;

import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.ServerData;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.Style;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.event.ClickEvent;
import net.minecraft.util.text.event.HoverEvent;
import net.minecraftforge.client.event.ClientChatReceivedEvent;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.network.FMLNetworkEvent;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ChatHandler {
    public static Logger log;
    String userName;
    private boolean serverIsVerified;
    private int PLAYER_NAME_PADDING;
    private int SERVER_TIMEZONE_UTC_OFFSET;
    private String SERVER_TIMEZONE_NAME;
    private int MAX_SNITCH_NAME_LENGTH;
    private TextFormatting reset;
    private TextFormatting italic;
    private TextFormatting black;
    private TextFormatting darkBlue;
    private TextFormatting darkGreen;
    private TextFormatting darkAqua;
    private TextFormatting darkRed;
    private TextFormatting darkPurple;
    private TextFormatting gold;
    private TextFormatting gray;
    private TextFormatting darkGray;
    private TextFormatting blue;
    private TextFormatting green;
    private TextFormatting aqua;
    private TextFormatting red;
    private TextFormatting lightPurple;
    private TextFormatting yellow;
    private TextFormatting white;
    HoverEvent hoverPM;
    private ITextComponent prefixPM;
    HoverEvent hoverHO;
    private ITextComponent prefixHO;
    HoverEvent hoverCT;
    private ITextComponent prefixCT;
    HoverEvent hoverSN;
    private ITextComponent prefixSN;
    HoverEvent hoverJA;
    private ITextComponent prefixJA;
    HoverEvent hoverNL;
    private ITextComponent prefixNL;
    HoverEvent hoverIE;
    private ITextComponent prefixIE;
    private ITextComponent prefixPVP;
    private Boolean suppressRegularPlayerLoginMessages;
    private String latestFirstTimePlayerName;
    private Boolean chatInPmMode;
    private ITextComponent itemExchangeExchangePrefix;

    public ChatHandler() {
        super();
        this.serverIsVerified = false;
        this.PLAYER_NAME_PADDING = 14;
        this.SERVER_TIMEZONE_UTC_OFFSET = -7;
        this.SERVER_TIMEZONE_NAME = "PDT";
        this.MAX_SNITCH_NAME_LENGTH = 40;
        this.reset = TextFormatting.RESET;
        this.italic = TextFormatting.ITALIC;
        this.black = TextFormatting.BLACK;
        this.darkBlue = TextFormatting.DARK_BLUE;
        this.darkGreen = TextFormatting.DARK_GREEN;
        this.darkAqua = TextFormatting.DARK_AQUA;
        this.darkRed = TextFormatting.DARK_RED;
        this.darkPurple = TextFormatting.DARK_PURPLE;
        this.gold = TextFormatting.GOLD;
        this.gray = TextFormatting.GRAY;
        this.darkGray = TextFormatting.DARK_GRAY;
        this.blue = TextFormatting.BLUE;
        this.green = TextFormatting.GREEN;
        this.aqua = TextFormatting.AQUA;
        this.red = TextFormatting.RED;
        this.lightPurple = TextFormatting.LIGHT_PURPLE;
        this.yellow = TextFormatting.YELLOW;
        this.white = TextFormatting.WHITE;
        this.hoverPM = new HoverEvent(HoverEvent.Action.SHOW_TEXT, (ITextComponent) new TextComponentString("Private Message"));
        this.prefixPM = new TextComponentString(this.lightPurple + "[PM]" + this.reset + "  ").setStyle(new Style().setHoverEvent(this.hoverPM));
        this.hoverHO = new HoverEvent(HoverEvent.Action.SHOW_TEXT, (ITextComponent) new TextComponentString("HiddenOre"));
        this.prefixHO = new TextComponentString(this.gray + "[HO]" + this.reset + "  ").setStyle(new Style().setHoverEvent(this.hoverHO));
        this.hoverCT = new HoverEvent(HoverEvent.Action.SHOW_TEXT, (ITextComponent) new TextComponentString("Citadel"));
        this.prefixCT = new TextComponentString(this.gray + "[CT]" + this.reset + "  ").setStyle(new Style().setHoverEvent(this.hoverCT));
        this.hoverSN = new HoverEvent(HoverEvent.Action.SHOW_TEXT, (ITextComponent) new TextComponentString("Snitch Notification"));
        this.prefixSN = new TextComponentString(this.darkPurple + "[SN]" + this.reset + "  ").setStyle(new Style().setHoverEvent(this.hoverSN));
        this.hoverJA = new HoverEvent(HoverEvent.Action.SHOW_TEXT, (ITextComponent) new TextComponentString("JukeAlert"));
        this.prefixJA = new TextComponentString(this.gray + "[JA]" + this.reset + "  ").setStyle(new Style().setHoverEvent(this.hoverJA));
        this.hoverNL = new HoverEvent(HoverEvent.Action.SHOW_TEXT, (ITextComponent) new TextComponentString("NameLayer"));
        this.prefixNL = new TextComponentString(this.gray + "[NL]" + this.reset + "  ").setStyle(new Style().setHoverEvent(this.hoverNL));
        this.hoverIE = new HoverEvent(HoverEvent.Action.SHOW_TEXT, (ITextComponent) new TextComponentString("ItemExchange"));
        this.prefixIE = new TextComponentString(this.gray + "[IE]" + this.reset + "  ").setStyle(new Style().setHoverEvent(this.hoverIE));
        this.prefixPVP = (ITextComponent) new TextComponentString(this.red + "[PvP]" + this.reset + "  ");
        this.suppressRegularPlayerLoginMessages = Boolean.valueOf(true);
        this.chatInPmMode = Boolean.valueOf(false);
    }

    @SubscribeEvent
    public void onServerJoin(final FMLNetworkEvent.ClientConnectedToServerEvent event) {
        if ((boolean) Chatfilters.DISABLED) {
            return;
        }
        if (event == null) {
            return;
        }
        final ServerData server = Minecraft.getMinecraft().getCurrentServerData();
        if (server == null) {
            this.serverIsVerified = false;
            return;
        }
        String serverIp;
        String serverPort;
        if (server.serverIP.contains(":")) {
            final String[] ipPort = server.serverIP.split(":");
            serverIp = ipPort[0];
            serverPort = ipPort[1];
            if (serverPort.trim().equals("")) {
                serverPort = "25565";
            }
        } else {
            serverIp = server.serverIP;
            serverPort = "25565";
        }
        ChatHandler.log.info(MessageFormat.format("{0}Connected to server {1}:{2}.", this.getLogPrefixINFO(), serverIp, serverPort));
//        if (serverIp.equals(" ") && serverPort.equals("25565")) {
//            ChatHandler.log.info(String.format("%sServer recognized as .", this.getLogPrefixINFO()));
//            this.serverIsVerified = true;
//            return;
//        }
//        ChatHandler.log.warn(String.format("%sServer not recognized, aborting. IP: '%s', Port: '%s'.", this.getLogPrefixWARNING(), serverIp, serverPort));
//        this.serverIsVerified = false;
    }

    @SubscribeEvent(priority = EventPriority.LOW)
    public void onChat(final ClientChatReceivedEvent event) {
        if (!this.serverIsVerified) {
            return;
        }
        if ((boolean) Chatfilters.DISABLED) {
            return;
        }
        if (event == null || event.getMessage() == null) {
            return;
        }
        final ITextComponent rawMessage = event.getMessage();
        if (rawMessage == null) {
            return;
        }
//        this.userName = Minecraft.getMinecraft().player.getName();
//        if (!this.userName.equals(" ")) {
//            return;
//        }
        final String message = rawMessage.getFormattedText();
        final String messageClean = this.stripMinecraftFormattingCodes(rawMessage.getUnformattedText());
        ChatHandler.log.info(String.format("%sUnformatted chat message: '%s'.", this.getLogPrefixINFO(), messageClean));
        ChatHandler.log.info(String.format("%sFormatted chat message:   '%s'.", this.getLogPrefixINFO(), message));
        ChatHandler.log.debug(String.format("%sChat message JSON: '%s'.", this.getLogPrefixINFO(), ITextComponent.Serializer.componentToJson(event.getMessage())));
        if ((boolean) this.doFilter(event, message, messageClean)) {
            return;
        }
        if ((boolean) this.doGray(event, message, messageClean)) {
            return;
        }
        if ((boolean) this.doDarkGray(event, message, messageClean)) {
            return;
        }
        if ((boolean) this.onUnknownCommandError(event, message, messageClean)) {
            return;
        }
        if (this.onServerMessage(event, message, messageClean)) {
            return;
        }
        if (this.onAdminMessage(event, message, messageClean)) {
            return;
        }
        if (this.onSpamWarning(event, message, messageClean)) {
            return;
        }
        if (this.onCombatTagOn(event, message, messageClean)) {
            return;
        }
        if (this.onCombatTagTimerOn(event, message, messageClean)) {
            return;
        }
        if (this.onCombatTagTimerOff(event, message, messageClean)) {
            return;
        }
        if (this.onCombatTagOff(event, message, messageClean)) {
            return;
        }
        if (this.onPvp(event, message, messageClean)) {
            return;
        }
        if (this.onXpBottleFill(event, message, messageClean)) {
            return;
        }
        if (this.onChatSwitchPermissionError(event, message, messageClean)) {
            return;
        }
        if (this.onChatSwitchToGroup(event, message, messageClean)) {
            return;
        }
        if (this.onChatSwitchToGroupAgain(event, message, messageClean)) {
            return;
        }
        if (this.onChatSwitchToNonexistentGroup(event, message, messageClean)) {
            return;
        }
        if (this.onGroupChatMessage(event, message, messageClean)) {
            return;
        }
        if (this.onChatSwitchToGlobal(event, message, messageClean)) {
            return;
        }
        if (this.onGlobalChatMessage(event, message, messageClean)) {
            return;
        }
        if (this.onChatSwitchToPm(event, message, messageClean)) {
            return;
        }
        if (this.onChatSwitchFromPm(event, message, messageClean)) {
            return;
        }
        if (this.onPmChatMessage(event, message, messageClean)) {
            return;
        }
        if (this.onMessageSelfError(event, message, messageClean)) {
            return;
        }
        if (this.onPmOfflinePlayerError(event, message, messageClean)) {
            return;
        }
        if (this.onChatIgnorelistNoPlayerIgnores(event, message, messageClean)) {
            return;
        }
        if (this.onChatIgnorelistNoGroupIgnores(event, message, messageClean)) {
            return;
        }
        if (this.onChatIgnoreAdd(event, message, messageClean)) {
            return;
        }
        if (this.onChatIgnoreRemove(event, message, messageClean)) {
            return;
        }
        if (this.onChatMessageIgnored(event, message, messageClean)) {
            return;
        }
        if (this.onHiddenOreDrop(event, message, messageClean)) {
            return;
        }
        if (this.onLockedBlock(event, message, messageClean)) {
            return;
        }
        if (this.onCtiOnFirst(event, message, messageClean)) {
            return;
        }
        if (this.onCtiOnSecond(event, message, messageClean)) {
            return;
        }
        if (this.onCti(event, message, messageClean)) {
            return;
        }
        if (this.onCtiOff(event, message, messageClean)) {
            return;
        }
        if (this.onReinforcementGroupPermissionError(event, message, messageClean)) {
            return;
        }
        if (this.onCtfOnFirst(event, message, messageClean)) {
            return;
        }
        if (this.onCtfOnSecond(event, message, messageClean)) {
            return;
        }
        if (this.onCtfOff(event, message, messageClean)) {
            return;
        }
        if (this.onCtfOffMaterialDepleted(event, message, messageClean)) {
            return;
        }
        if (this.onCtfEmptyHandError(event, message, messageClean)) {
            return;
        }
        if (this.onCtfWrongMaterialError(event, message, messageClean)) {
            return;
        }
        if (this.onReinforceGroupDoesntExistError(event, message, messageClean)) {
            return;
        }
        if (this.onReinfoceWrongMaterialBlockCombinationError(event, message, messageClean)) {
            return;
        }
        if (this.onCtrOnFirst(event, message, messageClean)) {
            return;
        }
        if (this.onCtrOnSecond(event, message, messageClean)) {
            return;
        }
        if (this.onCtrOff(event, message, messageClean)) {
            return;
        }
        if (this.onCtrGroupChange(event, message, messageClean)) {
            return;
        }
        if (this.onCtrMaterialChange(event, message, messageClean)) {
            return;
        }
        if (this.onCtrUnreinforceableBlockError(event, message, messageClean)) {
            return;
        }
        if (this.onCtrNoPermissionError(event, message, messageClean)) {
            return;
        }
        if (this.onCtrWrongMaterialError(event, message, messageClean)) {
            return;
        }
        if (this.onCtbOn(event, message, messageClean)) {
            return;
        }
        if (this.onCtbOff(event, message, messageClean)) {
            return;
        }
        if (this.onCtbOffWarning(event, message, messageClean)) {
            return;
        }
        if (this.onCteOn(event, message, messageClean)) {
            return;
        }
        if (this.onCteOff(event, message, messageClean)) {
            return;
        }
        if (this.onSnitchNotification(event, message, messageClean)) {
            return;
        }
        if (this.onUnreinforcedSnitchPlace(event, message, messageClean)) {
            return;
        }
        if (this.onSnitchPlace(event, message, messageClean, rawMessage)) {
            return;
        }
        if (this.onJaname(event, message, messageClean, rawMessage)) {
            return;
        }
        if (this.onJalookupPermissionError(event, message, messageClean)) {
            return;
        }
        if (this.onJalookup(event, message, messageClean)) {
            return;
        }
        if (this.onJaclear(event, message, messageClean)) {
            return;
        }
        if (this.onJainfoNoSnitchesError(event, message, messageClean)) {
            return;
        }
        if (this.onJainfoEntrySnitch(event, message, messageClean)) {
            return;
        }
        if (this.onJainfoNoLogs(event, message, messageClean)) {
            return;
        }
        if (this.onJainfoLogs(event, message, messageClean)) {
            return;
        }
        if (this.onJainfoLogsPagination(event, message, messageClean)) {
            return;
        }
        if (this.onJainfoKill(event, message, messageClean)) {
            return;
        }
        if (this.onJainfoEntryLoginLogout(event, message, messageClean)) {
            return;
        }
        if (this.onJainfoBlockUse(event, message, messageClean)) {
            return;
        }
        if (this.onJainfoEntityUse(event, message, messageClean)) {
            return;
        }
        if (this.onJainfoBucketUse(event, message, messageClean)) {
            return;
        }
        if (this.onJainfoBlockChange(event, message, messageClean)) {
            return;
        }
        if (this.onJamuteNoIgnores(event, message, messageClean)) {
            return;
        }
        if (this.onJamuteList(event, message, messageClean)) {
            return;
        }
        if (this.onJamuteAddFirst(event, message, messageClean)) {
            return;
        }
        if (this.onJamuteAddSecond(event, message, messageClean)) {
            return;
        }
        if (this.onJamuteRemove(event, message, messageClean)) {
            return;
        }
        if (this.onNonOwnedDestruction(event, message, messageClean)) {
            return;
        }
        if (this.onCommandNoPermissionError(event, message, messageClean)) {
            return;
        }
        if (this.onGroupModifyNoPermissionError(event, message, messageClean)) {
            return;
        }
        if (this.onGroupDoesntExistError(event, message, messageClean)) {
            return;
        }
        if (this.onPlayerHasNeverPlayedBeforeError(event, message, messageClean)) {
            return;
        }
        if (this.onNlcgGroupExistsError(event, message, messageClean)) {
            return;
        }
        if (this.onNlcgGroupNameTooLongError(event, message, messageClean)) {
            return;
        }
        if (this.onNlcgTooManyGroupsWarning(event, message, messageClean)) {
            return;
        }
        if (this.onNlcgTooManyGroupsError(event, message, messageClean)) {
            return;
        }
        if (this.onNlcgScheduled(event, message, messageClean)) {
            return;
        }
        if (this.onNlcgSuccess(event, message, messageClean)) {
            return;
        }
        if (this.onNldgConfirmationFirst(event, message, messageClean)) {
            return;
        }
        if (this.onNldgConfirmationSecond(event, message, messageClean)) {
            return;
        }
        if (this.onNldgConfirmationTimeoutError(event, message, messageClean)) {
            return;
        }
        if (this.onNldgGuiSuccess(event, message, messageClean)) {
            return;
        }
        if (this.onNldgSuccess(event, message, messageClean)) {
            return;
        }
        if (this.onNllmFirst(event, message, messageClean)) {
            return;
        }
        if (this.onNllmSecond(event, message, messageClean)) {
            return;
        }
        if (this.onNllgPagination(event, message, messageClean)) {
            return;
        }
        if (this.onNllg(event, message, messageClean)) {
            return;
        }
        if (this.onNlipPlayerAlreadyAMemberError(event, message, messageClean)) {
            return;
        }
        if (this.onNlipFirst(event, message, messageClean)) {
            return;
        }
        if (this.onNlipSecond(event, message, messageClean)) {
            return;
        }
        if (this.onNlipReceive(event, message, messageClean)) {
            return;
        }
        if (this.onNlipReceiveOffline(event, message, messageClean)) {
            return;
        }
        if (this.onNlriAlreadyAcceptedError(event, message, messageClean)) {
            return;
        }
        if (this.onNlagNotInvitedError(event, message, messageClean)) {
            return;
        }
        if (this.onNlagSuccess(event, message, messageClean)) {
            return;
        }
        if (this.onNlrmPlayerNotInGroupError(event, message, messageClean)) {
            return;
        }
        if (this.onNlrmSuccess(event, message, messageClean)) {
            return;
        }
        if (this.onNlpp(event, message, messageClean)) {
            return;
        }
        if (this.onNlppReceive(event, message, messageClean)) {
            return;
        }
        if (this.onNllp(event, message, messageClean)) {
            return;
        }
        if (this.onNlmpGroupHasPermError(event, message, messageClean)) {
            return;
        }
        if (this.onNlmpGroupDoesntHavePermError(event, message, messageClean)) {
            return;
        }
        if (this.onNlmp(event, message, messageClean)) {
            return;
        }
        if (this.onNlleg(event, message, messageClean)) {
            return;
        }
        if (this.onIeFirst(event, message, messageClean)) {
            return;
        }
        if (this.onIeSecond(event, message, messageClean)) {
            return;
        }
        if (this.onIeThird(event, message, messageClean)) {
            return;
        }
        if (this.onIeFourth(event, message, messageClean)) {
            return;
        }
        if (this.onIeExchangeNoItemsLeftError(event, message, messageClean)) {
            return;
        }
        if (this.onIeExchangeSuccess(event, message, messageClean)) {
            return;
        }
        if (this.onIeExchangeCreationSuccess(event, message, messageClean)) {
            return;
        }
    }

    private String stripMinecraftFormattingCodes(final String str) {
        return str.replaceAll("(?i)\\u00A7[a-z0-9]", "");
    }

    private String getLogPrefixDEBUG() {
        return String.format("[Chatfilters DEBUG %s] ", this.getDate());
    }

    private String getLogPrefixINFO() {
        return String.format("[Chatfilters INFO %s] ", this.getDate());
    }

    private String getLogPrefixWARNING() {
        return String.format("[Chatfilters WARNING %s] ", this.getDate());
    }

    private String getLogPrefixERROR() {
        return String.format("[Chatfilters ERROR %s] ", this.getDate());
    }

    private String getDate() {
        final DateFormat d = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        d.setTimeZone(TimeZone.getTimeZone("UTC"));
        return d.format(new Date());
    }

    private Boolean doFilter(final ClientChatReceivedEvent event, final String message, final String messageClean) {
        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^(?: +$|>>>$|Timeout between commands is [0-9]|You have been combat tagged on login|Reinforcement mode has been set to normal|normal mode off)");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return Boolean.valueOf(false);
        }
        ChatHandler.log.info(String.format("%s%s: Chat message filtered. Message: '%s'.", this.getLogPrefixINFO(), selfName, messageClean));
        event.setCanceled(true);
        return Boolean.valueOf(true);
    }

    private Boolean doGray(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*(?:-*? Help:|Command:|Description:|Usage:|You can only sleep at night|You have earned dedicated status|\\$[0-9]+\\s*=\\s*[a-z])\\.?");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return Boolean.valueOf(false);
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling((ITextComponent) new TextComponentString(this.gray + messageClean + this.reset + " "));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return Boolean.valueOf(true);
    }

    private Boolean doDarkGray(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*(?:all times are ...)");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return Boolean.valueOf(false);
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}{1}{2} ", this.darkGray, messageClean, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return Boolean.valueOf(true);
    }

    private Boolean onUnknownCommandError(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^Did you type that right\\?$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.matches()) {
            return Boolean.valueOf(false);
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Error:  {1}Unrecognized command. Type {2}/help {3}for a list of available commands.{4} ", this.red, this.gray, this.white, this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return Boolean.valueOf(true);
    }

    private boolean onServerMessage(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*\\[Server\\] (.+?)$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.matches()) {
            return false;
        }
        if (match.group(1) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final String serverMessage = match.group(1);
        final ITextComponent handledServerMessage = this.handleChatMessage(serverMessage);
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}[Server]  {1}", this.darkRed, this.white))).appendSibling(handledServerMessage).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0} ", this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onAdminMessage(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        Pattern regex = Pattern.compile("(?i)^\\s*\\u00A7r\\u00A76\\u00A7l\\[Owner\\]\\u00A7r.*?$");
        Matcher match = regex.matcher(message);
        if (!match.matches()) {
            regex = Pattern.compile("(?i)^\\s*\\[\\u00A7r(?:ryan00793|Epsilon)\\u00A7r\\].*?$");
            match = regex.matcher(message);
            if (!match.matches()) {
                return false;
            }
        }
        regex = Pattern.compile("(?i)^\\s*\\[?Owner\\]?\\s*(.+?):\\*?(.*?)$");
        match = regex.matcher(messageClean);
        if (!match.matches()) {
            regex = Pattern.compile("(?i)^\\s*\\[(ryan00793|Epsilon)\\]\\s*(.*?)$");
            match = regex.matcher(messageClean);
            if (!match.matches()) {
                return false;
            }
        }
        if (match.group(1) == null || match.group(2) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final String adminName = match.group(1);
        final String adminMessage = match.group(2);
        final ITextComponent handledAdminMessage = this.handleChatMessage(adminMessage);
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}[Admin]  {1}{2}  {3}", this.darkRed, this.darkAqua, adminName, this.white))).appendSibling(handledAdminMessage).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0} ", this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onSpamWarning(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*>>> Please slow down chat, you might get kicked for spam\\.?$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.matches()) {
            return false;
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Warning:  {1}Excessive rate of chat messages.{2} ", this.gold, this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onCombatTagOn(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*You have engaged in combat with (.+?)\\.? Type /ct to check your timer\\.?$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.matches()) {
            return false;
        }
        if (match.group(1) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final String playerName = match.group(1);
        final double playerCoordX = Minecraft.getMinecraft().player.posX;
        final double playerCoordY = Minecraft.getMinecraft().player.posY;
        final double playerCoordZ = Minecraft.getMinecraft().player.posZ;
        final String coords = MessageFormat.format("{0,number,#} {1,number,#} {2,number,#}", Double.valueOf(playerCoordX), Double.valueOf(playerCoordY), Double.valueOf(playerCoordZ));
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        if (playerName.toLowerCase().equals("someone")) {
            msg = msg.appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Combat tag  {1}ON{2} ", this.gray, this.red, this.reset)));
        } else {
            final int playerDimensionId = Minecraft.getMinecraft().world.provider.getDimension();
            final ITextComponent jmWaypointFmt = this.getClickableJmWaypointFmt(playerCoordX, Double.valueOf(playerCoordY), playerCoordZ, MessageFormat.format("CombatTag-{0}", playerName), Integer.valueOf(playerDimensionId));
            msg = msg.appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Combat tag  {1}ON  {2}{3}  {4}[{5}]  ", this.gray, this.red, this.darkAqua, playerName, this.gray, coords))).appendSibling(jmWaypointFmt).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0} ", this.reset)));
        }
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onCombatTagTimerOn(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*([0-9]+) seconds? remaining on your combat timer\\.?$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.matches()) {
            return false;
        }
        if (match.group(1) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final int timeSeconds = Integer.parseInt(match.group(1));
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Combat tag:  {1}{2}s{3} ", this.gray, this.red, Integer.valueOf(timeSeconds), this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onCombatTagTimerOff(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*You are not in combat\\.?$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.matches()) {
            return false;
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Not combat tagged.{1} ", this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onCombatTagOff(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*You are no longer in combat\\.?$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.matches()) {
            return false;
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Combat tag  {1}OFF{2} ", this.gray, this.green, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onPvp(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        Pattern regex = Pattern.compile("(?i)^\\s*>>");
        Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        regex = Pattern.compile("(?i)^\\s*>>\\s*(.+?) was slain by (.+?) \\[(.+?)\\]\\s*$");
        match = regex.matcher(messageClean);
        if (match.matches()) {
            if (match.group(1) == null || match.group(2) == null || match.group(3) == null) {
                ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
                return true;
            }
            final String defenderName = match.group(1);
            final String attackerName = match.group(2);
            final String weaponName = match.group(3);
            msg = msg.appendSibling(this.prefixPVP).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}{1} {2}killed {3}{4} {5}with {6}{7} ", this.white, attackerName, this.gray, this.white, defenderName, this.gray, weaponName, this.reset)));
            Minecraft.getMinecraft().player.sendMessage(msg);
            event.setCanceled(true);
            return true;
        } else {
            regex = Pattern.compile("(?i)^\\s*>>\\s*(.+?) was slain by (.+?) with a Bow\\s*$");
            match = regex.matcher(messageClean);
            if (match.matches()) {
                if (match.group(1) == null || match.group(2) == null) {
                    ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
                    return true;
                }
                final String defenderName = match.group(1);
                final String attackerName = match.group(2);
                msg = msg.appendSibling(this.prefixPVP).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}{1} {2}shot {3}{4}{5} ", this.white, attackerName, this.gray, this.white, defenderName, this.reset)));
                Minecraft.getMinecraft().player.sendMessage(msg);
                event.setCanceled(true);
                return true;
            } else {
                regex = Pattern.compile("(?i)^\\s*>>\\s*(.+?) was slain by (.+?) with a (.+?)\\s*$");
                match = regex.matcher(messageClean);
                if (match.matches()) {
                    if (match.group(1) == null || match.group(2) == null || match.group(3) == null) {
                        ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
                        return true;
                    }
                    final String defenderName = match.group(1);
                    final String attackerName = match.group(2);
                    final String weaponName = match.group(3);
                    msg = msg.appendSibling(this.prefixPVP).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}{1} {2}killed {3}{4} {5}with {6}{7} ", this.white, attackerName, this.gray, this.white, defenderName, this.gray, weaponName, this.reset)));
                    Minecraft.getMinecraft().player.sendMessage(msg);
                    event.setCanceled(true);
                    return true;
                } else {
                    regex = Pattern.compile("(?i)^\\s*>>\\s*(.+?) was slain by (.+?)\\s*$");
                    match = regex.matcher(messageClean);
                    if (match.matches()) {
                        if (match.group(1) == null || match.group(2) == null) {
                            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
                            return true;
                        }
                        final String defenderName = match.group(1);
                        final String attackerName = match.group(2);
                        msg = msg.appendSibling(this.prefixPVP).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}{1} {2}killed {3}{4}{5} ", this.white, attackerName, this.gray, this.white, defenderName, this.reset)));
                        Minecraft.getMinecraft().player.sendMessage(msg);
                        event.setCanceled(true);
                        return true;
                    } else {
                        regex = Pattern.compile("(?i)^\\s*>>\\s*(.+?) was shot by (.+?) using \\[(.+?)\\]\\s*$");
                        match = regex.matcher(messageClean);
                        if (!match.matches()) {
                            return false;
                        }
                        if (match.group(1) == null || match.group(2) == null || match.group(3) == null) {
                            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
                            return true;
                        }
                        final String defenderName = match.group(1);
                        final String attackerName = match.group(2);
                        final String weaponName = match.group(3);
                        msg = msg.appendSibling(this.prefixPVP).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}{1} {2}shot {3}{4} {5}with {6}{7} ", this.white, attackerName, this.gray, this.white, defenderName, this.gray, weaponName, this.reset)));
                        Minecraft.getMinecraft().player.sendMessage(msg);
                        event.setCanceled(true);
                        return true;
                    }
                }
            }
        }
    }

    private boolean onXpBottleFill(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*Created ([0-9]+?) XP bottles\\.?\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.matches()) {
            return false;
        }
        if (match.group(1) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final int bottleCount = Integer.parseInt(match.group(1));
        String s;
        if (bottleCount == 1) {
            s = "";
        } else {
            s = "s";
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Filled {1}{2} {3}bottle{4} with XP.{5} ", this.gray, this.white, Integer.valueOf(bottleCount), this.gray, s, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onChatSwitchPermissionError(final ClientChatReceivedEvent event, final String message, final String messageClean) {
        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*You don't have permission to chat in this group\\.?$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.matches()) {
            return false;
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Error:  {1}No permission to chat in that group.{2} ", this.red, this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onChatSwitchToGroup(final ClientChatReceivedEvent event, final String message, final String messageClean) {
        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("^\\s*You are now chatting in group (.+?)\\.?$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.matches()) {
            return false;
        }
        if (match.group(1) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final String groupName = match.group(1);
        final ClickEvent runCommand = new ClickEvent(ClickEvent.Action.RUN_COMMAND, MessageFormat.format("/gc {0}", groupName));
        final ITextComponent runCommandFmt = new TextComponentString(MessageFormat.format("{0}<{1}>", this.gold, groupName)).setStyle(new Style().setClickEvent(runCommand));
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Chat:  ", this.white))).appendSibling(runCommandFmt).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0} ", this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        if ((boolean) this.chatInPmMode) {
            this.chatInPmMode = Boolean.valueOf(false);
            Minecraft.getMinecraft().player.sendChatMessage("/m");
        }
        event.setCanceled(true);
        return true;
    }

    private boolean onChatSwitchToGroupAgain(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*You are already chatting in that group\\.?$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.matches()) {
            return false;
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Already chatting in that group.{1} ", this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        if ((boolean) this.chatInPmMode) {
            this.chatInPmMode = Boolean.valueOf(false);
            Minecraft.getMinecraft().player.sendChatMessage("/m");
        }
        event.setCanceled(true);
        return true;
    }

    private boolean onChatSwitchToNonexistentGroup(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*There is no group with that name\\.?$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.matches()) {
            return false;
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Error:  {1}That group doesn''t exist.{2} ", this.red, this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        if ((boolean) this.chatInPmMode) {
            this.chatInPmMode = Boolean.valueOf(false);
            Minecraft.getMinecraft().player.sendChatMessage("/m");
        }
        event.setCanceled(true);
        return true;
    }

    private boolean onGroupChatMessage(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("^\\s*\\u00A7r\\s*\\u00A78\\[(.+?)\\] (\\S+?): (.+?)$");
        final Matcher match = regex.matcher(message);
        if (!match.matches()) {
            return false;
        }
        if (match.group(1) == null || match.group(2) == null || match.group(3) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, message));
            return true;
        }
        final String groupName = this.stripMinecraftFormattingCodes(match.group(1));
        final String playerName = this.stripMinecraftFormattingCodes(match.group(2));
        final String playerMessage = this.stripMinecraftFormattingCodes(match.group(3));
        final ITextComponent playerNameFmt = this.getFormattedPlayerName(playerName);
        final ITextComponent handledPlayerMessage = this.handleChatMessage(playerMessage);
        final ClickEvent runCommand = new ClickEvent(ClickEvent.Action.RUN_COMMAND, MessageFormat.format("/gc {0}", groupName));
        final ITextComponent runCommandFmt = new TextComponentString(MessageFormat.format("{0}<{1}>", this.gold, groupName)).setStyle(new Style().setClickEvent(runCommand));
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(runCommandFmt).appendSibling((ITextComponent) new TextComponentString("  ")).appendSibling(playerNameFmt).appendSibling((ITextComponent) new TextComponentString("  ")).appendSibling(handledPlayerMessage).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0} ", this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        final List<ITextComponent> msgs = this.handleCoordinatesInMessage(playerMessage, null);
        for (final ITextComponent coordsMsg : msgs) {
            Minecraft.getMinecraft().player.sendMessage(coordsMsg);
        }
        event.setCanceled(true);
        return true;
    }

    private ITextComponent getFormattedPlayerName(final String playerName) {
        final ClickEvent action = new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, MessageFormat.format("/msg {0} ", playerName));
        return new TextComponentString(MessageFormat.format("{0}{1}", this.darkAqua, playerName)).setStyle(new Style().setClickEvent(action));
    }

    private ITextComponent handleChatMessage(final String message) {
        final ITextComponent msg = (ITextComponent) new TextComponentString("");
        final String userNameRegex = this.userNameToRegex(this.userName);
        Boolean greentext = Boolean.valueOf(false);
        ITextComponent wordComponent = (ITextComponent) new TextComponentString("");
        for (final String word : message.split(" ")) {
            if (word.startsWith(">") || greentext) {
                greentext = Boolean.valueOf(true);
                wordComponent = (ITextComponent) new TextComponentString(this.green + word + " ");
            }
            if (!(boolean) greentext) {
                final Pattern regex = Pattern.compile(String.format("(?i)^()(%s)($|:|\\.+|,|\\?+|!+)$", userNameRegex));
                final Matcher match = regex.matcher(word);
                if (match.matches()) {
                    final String p1 = match.group(1);
                    final String p2 = match.group(2);
                    final String p3 = match.group(3);
                    wordComponent = (ITextComponent) new TextComponentString(this.white + p1 + this.lightPurple + p2 + this.white + p3 + " ");
                } else {
                    wordComponent = (ITextComponent) new TextComponentString(this.white + word + " ");
                }
            }
            final Pattern regex = Pattern.compile("(https?://.+)");
            final Matcher match = regex.matcher(word);
            if (match.find()) {
                final String url = match.group(1);
                final HoverEvent hoverUrl = new HoverEvent(HoverEvent.Action.SHOW_TEXT, (ITextComponent) new TextComponentString(url));
                final ClickEvent actionUrl = new ClickEvent(ClickEvent.Action.OPEN_URL, url);
                wordComponent = wordComponent.setStyle(new Style().setClickEvent(actionUrl).setHoverEvent(hoverUrl));
            }
            msg.appendSibling(wordComponent);
        }
        return msg;
    }

    private List<ITextComponent> handleCoordinatesInMessage(final String message, final String waypointName) {

        final String selfName = "ChatFilter";
        final List<MessageDistance> messageDistances = new ArrayList<MessageDistance>();
        Pattern regex = Pattern.compile("\\[\\s*(world|world_nether|world_the_end)\\s+(-?[0-9]+)(?:\\s+|\\s*,\\s*)(-?[0-9]+)\\s*\\]");
        List<MessageDistance> result = this.getMatchingCoordinates(regex, true, message, waypointName, false, true);
        messageDistances.addAll(result);
        regex = Pattern.compile("\\[\\s*(world|world_nether|world_the_end)\\s+(-?[0-9]+)(?:\\s+|\\s*,\\s*)(-?[0-9]+)(?:\\s+|\\s*,\\s*)(-?[0-9]+)\\s*\\]");
        result = this.getMatchingCoordinates(regex, false, message, waypointName, false, true);
        messageDistances.addAll(result);
        regex = Pattern.compile("\\[\\s*(-?[0-9]+)(?:\\s+|\\s*,\\s*)(-?[0-9]+)\\s*\\]");
        result = this.getMatchingCoordinates(regex, true, message, waypointName, false, false);
        messageDistances.addAll(result);
        regex = Pattern.compile("\\[\\s*(-?[0-9]+)(?:\\s+|\\s*,\\s*)(-?[0-9]+)(?:\\s+|\\s*,\\s*)(-?[0-9]+)\\s*\\]");
        result = this.getMatchingCoordinates(regex, false, message, waypointName, false, false);
        messageDistances.addAll(result);
        regex = Pattern.compile("\\[?\\s*x(?:=|:)(-?[0-9]+)(?:\\s+|\\s*,\\s*)z(?:=|:)(-?[0-9]+)\\s*\\]?");
        result = this.getMatchingCoordinates(regex, true, message, waypointName, false, false);
        messageDistances.addAll(result);
        regex = Pattern.compile("\\[?\\s*x(?:=|:)(-?[0-9]+)(?:\\s+|\\s*,\\s*)y(?:=|:)(-?[0-9]+)(?:\\s+|\\s*,\\s*)z(?:=|:)(-?[0-9]+)\\s*\\]?");
        result = this.getMatchingCoordinates(regex, false, message, waypointName, false, false);
        messageDistances.addAll(result);
        regex = Pattern.compile("(?<![0-9],?)(?:^|\\s+)(-?[0-9]+)(?:\\s+|\\s*,\\s+)(-?[0-9]+)(?:\\s+|$)(?!,?[0-9-])");
        result = this.getMatchingCoordinates(regex, true, message, waypointName, false, false);
        messageDistances.addAll(result);
        regex = Pattern.compile("(?:^|\\s)(-?[0-9]+)(?:\\s+|\\s*,\\s+)(-?[0-9]+)(?:\\s+|\\s*,\\s+)(-?[0-9]+)(?:\\s|$)");
        result = this.getMatchingCoordinates(regex, false, message, waypointName, false, false);
        messageDistances.addAll(result);
        regex = Pattern.compile("(?:^|\\[\\s*|\\s+)(-?[0-9]+\\.?[0-9]*)k(?:\\s+|\\s*,\\s*)(-?[0-9]+\\.?[0-9]*)k\\s*(?:\\s+|\\s*\\]|$)");
        result = this.getMatchingCoordinates(regex, true, message, waypointName, true, false);
        messageDistances.addAll(result);
        Collections.<MessageDistance>sort(messageDistances);
        final List<MessageDistance> messageDistancesDedup = new ArrayList<MessageDistance>();
        for (final MessageDistance msgd : messageDistances) {
            boolean equalCoords = false;
            for (final MessageDistance msgdd : messageDistancesDedup) {
                if ((int) msgd.coordX == (int) msgdd.coordX && (int) msgd.coordY == (int) msgdd.coordY && (int) msgd.coordZ == (int) msgdd.coordZ) {
                    equalCoords = true;
                    break;
                }
            }
            if (equalCoords) {
                continue;
            }
            messageDistancesDedup.add(msgd);
        }
        final List<ITextComponent> out = new ArrayList<ITextComponent>();
        for (final MessageDistance msgdd2 : messageDistancesDedup) {
            out.add(msgdd2.message);
        }
        return out;
    }

    private List<MessageDistance> getMatchingCoordinates(final Pattern regex, final boolean onlyXZ, final String message, final String waypointName, final boolean k, final boolean world) {

        final String selfName = "ChatFilter";
        final List<MessageDistance> out = new ArrayList<MessageDistance>();
        final MessageDistance messageDistance = new MessageDistance();
        final double playerCoordX = Minecraft.getMinecraft().player.posX;
        final double playerCoordY = Minecraft.getMinecraft().player.posY;
        final double playerCoordZ = Minecraft.getMinecraft().player.posZ;
        final Matcher match = regex.matcher(message);
        while (match.find()) {
            int coordDimensionId = 0;
            double coordX;
            double coordY;
            double coordZ;
            if (world) {
                final String world_name = match.group(1);
                if (onlyXZ) {
                    coordX = Double.parseDouble(match.group(2));
                    coordY = 0.0;
                    coordZ = Double.parseDouble(match.group(3));
                } else {
                    coordX = Double.parseDouble(match.group(2));
                    coordY = Double.parseDouble(match.group(3));
                    coordZ = Double.parseDouble(match.group(4));
                }
                final String s = world_name;
                int n = -1;
                switch (s.hashCode()) {
                    case 113318802: {
                        if (s.equals("world")) {
                            n = 0;
                            break;
                        }
                        break;
                    }
                    case 1865466277: {
                        if (s.equals("world_nether")) {
                            n = 1;
                            break;
                        }
                        break;
                    }
                    case -1198266272: {
                        if (s.equals("world_the_end")) {
                            n = 2;
                            break;
                        }
                        break;
                    }
                }
                switch (n) {
                    case 0: {
                        coordDimensionId = 0;
                        break;
                    }
                    case 1: {
                        coordDimensionId = -1;
                        break;
                    }
                    case 2: {
                        coordDimensionId = 1;
                        break;
                    }
                    default: {
                        ChatHandler.log.error(String.format("%s%s: Unrecognized coord world name. Name: '%s'.", this.getLogPrefixERROR(), selfName, world_name));
                        break;
                    }
                }
            } else if (onlyXZ) {
                coordX = Double.parseDouble(match.group(1));
                coordY = 0.0;
                coordZ = Double.parseDouble(match.group(2));
            } else {
                coordX = Double.parseDouble(match.group(1));
                coordY = Double.parseDouble(match.group(2));
                coordZ = Double.parseDouble(match.group(3));
            }
            if (coordY < 0.0) {
                ChatHandler.log.warn(String.format("%s%s: y < 0 in coordinates. y: %f", this.getLogPrefixWARNING(), selfName, Double.valueOf(coordY)));
            } else if (coordY > 256.0) {
                ChatHandler.log.warn(String.format("%s%s: y > 256 in coordinates. y: %f", this.getLogPrefixWARNING(), selfName, Double.valueOf(coordY)));
            } else {
                if (k) {
                    coordX *= 1000.0;
                    coordZ *= 1000.0;
                }
                final String coords = MessageFormat.format("{0,number,#} {1,number,#} {2,number,#}", Double.valueOf(coordX), Double.valueOf(coordY), Double.valueOf(coordZ));
                final ITextComponent coordsWhitespaceFmt = (ITextComponent) new TextComponentString("    ");
                ITextComponent coordsFmt = (ITextComponent) new TextComponentString(MessageFormat.format("{0}[{1}]", this.white, coords));
                final ClickEvent suggestCommandCoords = new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, MessageFormat.format("[{0}]", coords));
                coordsFmt = coordsFmt.setStyle(new Style().setClickEvent(suggestCommandCoords));
                final ITextComponent jmWaypointFmt = this.getClickableJmWaypointFmt(coordX, Double.valueOf(coordY), coordZ, waypointName, Integer.valueOf(coordDimensionId));
                final int playerDimensionId = Minecraft.getMinecraft().world.provider.getDimension();
                if (coordDimensionId != playerDimensionId) {
                    String coordDimensionName = null;
                    switch (coordDimensionId) {
                        case 0: {
                            coordDimensionName = "World";
                            break;
                        }
                        case -1: {
                            coordDimensionName = "Nether";
                            break;
                        }
                        case 1: {
                            coordDimensionName = "The End";
                            break;
                        }
                        default: {
                            ChatHandler.log.error(String.format("%s%s: Unrecognized coord dimension name ID. ID: '%d'.", this.getLogPrefixERROR(), selfName, Integer.valueOf(coordDimensionId)));
                            continue;
                        }
                    }
                    final ITextComponent msg = new TextComponentString("  ").appendSibling(coordsFmt).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("  {0}is in {1}{2}", this.gray, this.white, coordDimensionName))).appendSibling((ITextComponent) new TextComponentString("  ")).appendSibling(jmWaypointFmt).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0} ", this.reset)));
                    messageDistance.message = msg;
                    messageDistance.distance = null;
                    messageDistance.coordX = coordX;
                    messageDistance.coordY = coordY;
                    messageDistance.coordZ = coordZ;
                    out.add(messageDistance);
                } else {
                    final double distance = this.getDistance(playerCoordX, playerCoordY, playerCoordZ, coordX, coordY, coordZ);
                    TextFormatting distanceColor;
                    if (distance <= 100.0) {
                        distanceColor = this.red;
                    } else if (distance <= 300.0) {
                        distanceColor = this.gold;
                    } else {
                        distanceColor = this.white;
                    }
                    final ITextComponent distanceFmt = (ITextComponent) new TextComponentString(MessageFormat.format("{0}{1}m", distanceColor, String.valueOf(Math.round(distance))));
                    final double yaw = this.getYaw(playerCoordX, playerCoordZ, coordX, coordZ);
                    ChatHandler.log.debug(String.format("%syaw = %f = getYaw(x1:%f, z1:%f, x2:%f, z2:%f).", this.getLogPrefixDEBUG(), Double.valueOf(yaw), Double.valueOf(playerCoordX), Double.valueOf(playerCoordZ), Double.valueOf(coordX), Double.valueOf(coordZ)));
                    if (yaw < 0.0 || yaw > 360.0) {
                        ChatHandler.log.error(String.format("%s%s: Yaw out of range for yaw = %f = getYaw(x1:%f, z1:%f, x2:%f, z2:%f).", this.getLogPrefixERROR(), selfName, Double.valueOf(yaw), Double.valueOf(playerCoordX), Double.valueOf(playerCoordZ), Double.valueOf(coordX), Double.valueOf(coordZ)));
                    } else {
                        String direction;
                        if (yaw >= 354.37 || yaw < 11.25) {
                            direction = "N";
                        } else if (yaw >= 11.25 && yaw < 33.75) {
                            direction = "NNE";
                        } else if (yaw >= 33.75 && yaw < 56.25) {
                            direction = "NE";
                        } else if (yaw >= 56.25 && yaw < 78.75) {
                            direction = "ENE";
                        } else if (yaw >= 78.75 && yaw < 101.25) {
                            direction = "E";
                        } else if (yaw >= 101.25 && yaw < 123.75) {
                            direction = "ESE";
                        } else if (yaw >= 123.75 && yaw < 146.25) {
                            direction = "SE";
                        } else if (yaw >= 146.25 && yaw < 168.75) {
                            direction = "SSE";
                        } else if (yaw >= 168.75 && yaw < 191.25) {
                            direction = "S";
                        } else if (yaw >= 191.25 && yaw < 213.75) {
                            direction = "SSW";
                        } else if (yaw >= 213.75 && yaw < 236.25) {
                            direction = "SW";
                        } else if (yaw >= 236.25 && yaw < 258.75) {
                            direction = "WSW";
                        } else if (yaw >= 258.75 && yaw < 281.25) {
                            direction = "W";
                        } else if (yaw >= 281.25 && yaw < 303.75) {
                            direction = "WNW";
                        } else if (yaw >= 303.75 && yaw < 326.25) {
                            direction = "NW";
                        } else {
                            if (yaw < 326.25 || yaw >= 354.37) {
                                ChatHandler.log.error(String.format("%s%s: Broken yaw -> direction if chain for yaw = %f = getYaw(x1:%f, z1:%f, x2:%f, z2:%f).", this.getLogPrefixERROR(), selfName, Double.valueOf(yaw), Double.valueOf(playerCoordX), Double.valueOf(playerCoordZ), Double.valueOf(coordX), Double.valueOf(coordZ)));
                                continue;
                            }
                            direction = "NNW";
                        }
                        final ITextComponent directionWhitespaceFmt = (ITextComponent) new TextComponentString("  ");
                        final ITextComponent directionFmt = (ITextComponent) new TextComponentString(MessageFormat.format("{0}{1}", distanceColor, this.mcChatRightPad(direction, 3, "  ")));
                        final ITextComponent msg2 = new TextComponentString("  ").appendSibling(coordsFmt).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("  {0}is  ", this.gray))).appendSibling(distanceFmt).appendSibling(directionWhitespaceFmt).appendSibling(directionFmt).appendSibling((ITextComponent) new TextComponentString("  ")).appendSibling(jmWaypointFmt).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0} ", this.reset)));
                        messageDistance.message = msg2;
                        messageDistance.distance = Double.valueOf(distance);
                        messageDistance.coordX = coordX;
                        messageDistance.coordY = coordY;
                        messageDistance.coordZ = coordZ;
                        out.add(messageDistance);
                    }
                }
            }
        }
        return out;
    }

    private String userNameToRegex(final String userName) {

        final String selfName = "ChatFilter";
        final List<String> tokens = new ArrayList<String>();
        for (final String word : userName.split("(?<!(^|[A-Z]))(?=[A-Z])|(?<!^)(?=[A-Z][a-z])")) {
            if (word.length() >= 4) {
                tokens.add(word);
            }
        }
        final String userNameLower = userName.toLowerCase();
        tokens.add(userNameLower);
        if (userNameLower.length() > 3) {
            tokens.add(userNameLower.substring(0, 4));
        }
        if (userNameLower.length() > 4) {
            tokens.add(userNameLower.substring(0, 5));
        }
        if (userNameLower.length() > 5) {
            tokens.add(userNameLower.substring(0, 6));
        }
        tokens.add(String.format("@%s.*", userNameLower));
        tokens.add(String.format("%s%s{1,30}", userNameLower, userNameLower.substring(userNameLower.length() - 1)));
        final StringBuilder sb = new StringBuilder();
        Boolean first = Boolean.valueOf(true);
        for (final String s : tokens) {
            if ((boolean) first) {
                first = Boolean.valueOf(false);
            } else {
                sb.append("|");
            }
            sb.append(s.toLowerCase());
        }
        final String regex = sb.toString();
        ChatHandler.log.debug(MessageFormat.format("{0}{1}: Tokens: {2}", this.getLogPrefixDEBUG(), selfName, regex));
        return regex;
    }

    private boolean onChatSwitchToGlobal(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*You are now in global chat\\.?$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.matches()) {
            return false;
        }
        final ClickEvent runCommand = new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/gc");
        final ITextComponent runCommandFmt = new TextComponentString(MessageFormat.format("{0}<global>", this.gray)).setStyle(new Style().setClickEvent(runCommand));
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Chat:  ", this.white))).appendSibling(runCommandFmt).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0} ", this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        if ((boolean) this.chatInPmMode) {
            this.chatInPmMode = Boolean.valueOf(false);
            Minecraft.getMinecraft().player.sendChatMessage("/m");
        }
        event.setCanceled(true);
        return true;
    }

    private boolean onGlobalChatMessage(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("^\\s*\\u00A7r<(\\S+?)> (.+?)$");
        final Matcher match = regex.matcher(message);
        if (!match.matches()) {
            return false;
        }
        if (match.group(1) == null || match.group(2) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, message));
            return true;
        }
        final String playerName = this.stripMinecraftFormattingCodes(match.group(1));
        final String playerMessage = this.stripMinecraftFormattingCodes(match.group(2));
        final ITextComponent playerNameFmt = this.getFormattedPlayerName(playerName);
        final ITextComponent handledPlayerMessage = this.handleChatMessage(playerMessage);
        final ClickEvent runCommand = new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/gc");
        final ITextComponent runCommandFmt = new TextComponentString(MessageFormat.format("{0}<g>", this.white)).setStyle(new Style().setClickEvent(runCommand));
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(runCommandFmt).appendSibling((ITextComponent) new TextComponentString("  ")).appendSibling(playerNameFmt).appendSibling((ITextComponent) new TextComponentString("  ")).appendSibling(handledPlayerMessage).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0} ", this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        final List<ITextComponent> msgs = this.handleCoordinatesInMessage(playerMessage, null);
        for (final ITextComponent coordsMsg : msgs) {
            Minecraft.getMinecraft().player.sendMessage(coordsMsg);
        }
        event.setCanceled(true);
        return true;
    }

    private boolean onChatSwitchToPm(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*you are now chatting with (.+?)\\.?$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.matches()) {
            return false;
        }
        if (match.group(1) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final String playerName = match.group(1);
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Chat:  {1}{2}{3} ", this.white, this.darkAqua, playerName, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        this.chatInPmMode = Boolean.valueOf(true);
        event.setCanceled(true);
        return true;
    }

    private boolean onChatSwitchFromPm(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*You left private chat\\.?$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.matches()) {
            return false;
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Left private chat.{1} ", this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        this.chatInPmMode = Boolean.valueOf(false);
        event.setCanceled(true);
        return true;
    }

    private boolean onPmChatMessage(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("^\\s*\\u00A7r\\s*\\u00A7d\\s*(To|From) (\\S+?): (.+?)$");
        final Matcher match = regex.matcher(message);
        if (!match.matches()) {
            return false;
        }
        if (match.group(1) == null || match.group(2) == null || match.group(3) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, message));
            return true;
        }
        final String messageType = this.stripMinecraftFormattingCodes(match.group(1));
        final String otherPlayerName = this.stripMinecraftFormattingCodes(match.group(2));
        final String playerMessage = this.stripMinecraftFormattingCodes(match.group(3));
        String senderPlayerName;
        if (messageType.toLowerCase().equals("to")) {
            senderPlayerName = this.userName;
        } else {
            if (!messageType.toLowerCase().equals("from")) {
                ChatHandler.log.error(String.format("%s%s: PM chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, message));
                return true;
            }
            senderPlayerName = otherPlayerName;
        }
        final ITextComponent otherPlayerNameFmt = this.getFormattedPlayerName(otherPlayerName);
        final ITextComponent handledPlayerMessage = this.handleChatMessage(playerMessage);
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixPM).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}<", this.lightPurple))).appendSibling(otherPlayerNameFmt).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}>  {1}{2}  ", this.lightPurple, this.darkAqua, senderPlayerName))).appendSibling(handledPlayerMessage).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0} ", this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        final List<ITextComponent> msgs = this.handleCoordinatesInMessage(playerMessage, null);
        for (final ITextComponent coordsMsg : msgs) {
            Minecraft.getMinecraft().player.sendMessage(coordsMsg);
        }
        event.setCanceled(true);
        return true;
    }

    private boolean onMessageSelfError(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*You can't message yourself\\.?$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.matches()) {
            return false;
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Error:  {1}Cannot message yourself.{2} ", this.red, this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onPmOfflinePlayerError(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*There is no player with that name\\.?$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.matches()) {
            return false;
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Error:  {1}No player with that name online.{2} ", this.red, this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onChatIgnorelistNoPlayerIgnores(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*You are not ignoring any players\\.?\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}No ignored players.{1} ", this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onChatIgnorelistNoGroupIgnores(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*You are not ignoring any groups\\.?\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}No ignored groups.{1} ", this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onChatIgnoreAdd(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("^\\s*You are now ignoring (.+?)$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.matches()) {
            return false;
        }
        if (match.group(1) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final String name = match.group(1);
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Muted chat from {1}{2}{3}.{4} ", this.gray, this.white, name, this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onChatIgnoreRemove(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("^\\s*You stopped ignoring (.+?)\\.?$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.matches()) {
            return false;
        }
        if (match.group(1) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final String name = match.group(1);
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Unmuted chat from {1}{2}{3}.{4} ", this.gray, this.white, name, this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onChatMessageIgnored(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("^\\s*You need to unignore (.+?)\\.?$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.matches()) {
            return false;
        }
        if (match.group(1) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final String name = match.group(1);
        final ClickEvent runIgnorePlayer = new ClickEvent(ClickEvent.Action.RUN_COMMAND, MessageFormat.format("/ignore {0}", name));
        final ITextComponent runIgnorePlayerFmt = new TextComponentString(MessageFormat.format("{0}/ignore {1}{2}", this.white, this.darkAqua, name)).setStyle(new Style().setClickEvent(runIgnorePlayer));
        final ClickEvent runIgnoreGroup = new ClickEvent(ClickEvent.Action.RUN_COMMAND, MessageFormat.format("/ignoregroup {0}", name));
        final ITextComponent runIgnoreGroupFmt = new TextComponentString(MessageFormat.format("{0}/ignoregroup {1}{2}", this.white, this.gold, name)).setStyle(new Style().setClickEvent(runIgnoreGroup));
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Error:  {1}Cannot send messages to ignored players/groups. To uningnore them, type ", this.red, this.gray))).appendSibling(runIgnorePlayerFmt).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format(" {0}or ", this.gray))).appendSibling(runIgnoreGroupFmt).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0} ", this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onHiddenOreDrop(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*You found a hidden ore! ([0-9]+?) (.+?)(Nearby|)$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.matches()) {
            return false;
        }
        if (match.group(1) == null || match.group(2) == null || match.group(3) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final int resourceAmount = Integer.parseInt(match.group(1));
        final String rawResourceName = match.group(2);
        final String messageType = match.group(3);
        final String resourceName = this.rawBlockNameToHumanReadable(rawResourceName);
        Boolean oreGenerated;
        if (messageType.equals("")) {
            oreGenerated = Boolean.valueOf(false);
        } else {
            oreGenerated = Boolean.valueOf(true);
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        if ((boolean) oreGenerated) {
            msg = msg.appendSibling(this.prefixHO).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Resources generated nearby:  {1}{2} {3}x {4}{5}{6} ", this.gray, this.white, Integer.valueOf(resourceAmount), this.gray, this.white, resourceName, this.reset)));
        } else {
            msg = msg.appendSibling(this.prefixHO).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Resources found:  {1}{2} {3}x {4}{5}{6} ", this.gray, this.white, Integer.valueOf(resourceAmount), this.gray, this.white, resourceName, this.reset)));
        }
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    public String rawBlockNameToHumanReadable(final String rawName) {
        String name = rawName.replace("_", " ").toLowerCase();
        name = WordUtils.capitalize(name);
        name = name.replace(" Item", "");
        name = name.replace(" Block", "");
        return name;
    }

    private boolean onLockedBlock(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("^(?i)\\s*(.+?) is locked\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.matches()) {
            return false;
        }
        if (match.group(1) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final String rawBlockName = match.group(1);
        final String blockName = this.rawBlockNameToHumanReadable(rawBlockName);
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixCT).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Error:  {1}That {2}{3} {4}is reinforced.{5} ", this.red, this.gray, this.white, blockName, this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onCtiOnFirst(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*You are now in Information mode, click on blocks to see their reinforcement information!\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixCT).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}/cti  {1}ON{2} ", this.gray, this.green, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onCtiOnSecond(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*Type /cti or /cto to turn this off\\.?\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        event.setCanceled(true);
        return true;
    }

    private boolean onCti(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*(?:reinforced|locked) ([0-9]+?\\.?[0-9]*?)% with ([a-z_]+?), (.+?)(?:,|$)(?: group: (.+?)(?: |$))?(?:\\((.+?)\\))?\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.matches()) {
            return false;
        }
        if (match.group(1) == null || match.group(2) == null || match.group(3) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final Double strengthPerc = Double.valueOf(Double.parseDouble(match.group(1)));
        final String rawMaterialName = match.group(2);
        final String decayStatus = match.group(3);
        final String groupName = match.group(4);
        final String hardeningStatus = match.group(5);
        TextFormatting strengthColor;
        if (strengthPerc >= 75.0) {
            strengthColor = this.darkAqua;
        } else if (strengthPerc >= 50.0) {
            strengthColor = this.green;
        } else if (strengthPerc >= 25.0) {
            strengthColor = this.gold;
        } else {
            if (strengthPerc < 0.0) {
                ChatHandler.log.error(String.format("%s%s: Reinforcement strength percentage out of bounds. Percentage: '%f'.", this.getLogPrefixERROR(), selfName, strengthPerc));
                return true;
            }
            strengthColor = this.red;
        }
        final ITextComponent strengthFmt = (ITextComponent) new TextComponentString(MessageFormat.format("{0}{1}%", strengthColor, this.mcChatLeftPad(String.valueOf(strengthPerc.intValue()), 4, "  ")));
        final String materialName = this.rawBlockNameToHumanReadable(rawMaterialName);
        ITextComponent materialNameFmt;
        int materialStrengthBreakCount;
        try {
            materialNameFmt = this.getFormattedReinforcementMaterial(materialName);
            materialStrengthBreakCount = this.getReinforcementMaterialStrengthBreakCount(materialName);
        } catch (IllegalArgumentException err) {
            ChatHandler.log.error(String.format("%s%s: Unknown reinforcement material. Material: '%s'.", this.getLogPrefixERROR(), selfName, materialName));
            return true;
        }
        final Double tmp = Double.valueOf(materialStrengthBreakCount * strengthPerc / 100.0);
        final int remainingBlockBreakCount = tmp.intValue();
        final ITextComponent remainingBlockBreakCountFmt = (ITextComponent) new TextComponentString(MessageFormat.format("{0}{1}", strengthColor, this.mcChatLeftPad(String.valueOf(remainingBlockBreakCount), 5, "  ")));
        ITextComponent hardeningStatusFmt;
        if (hardeningStatus == null) {
            hardeningStatusFmt = (ITextComponent) new TextComponentString("");
        } else {
            hardeningStatusFmt = (ITextComponent) new TextComponentString(MessageFormat.format("  {0}maturing", this.red));
        }
        ITextComponent groupNameFmt;
        if (groupName == null) {
            groupNameFmt = (ITextComponent) new TextComponentString(MessageFormat.format("{0}[{1}?{2}]", this.gold, this.red, this.gold));
        } else {
            final ClickEvent runCtf = new ClickEvent(ClickEvent.Action.RUN_COMMAND, MessageFormat.format("/ctf {0}", groupName));
            groupNameFmt = new TextComponentString(MessageFormat.format("{0}[{1}]", this.gold, groupName)).setStyle(new Style().setClickEvent(runCtf));
        }
        final String lowerCase = decayStatus.toLowerCase();
        int n = -1;
        switch (lowerCase.hashCode()) {
            case 1831756172: {
                if (lowerCase.equals("not decayed")) {
                    n = 0;
                    break;
                }
                break;
            }
            case 683083399: {
                if (lowerCase.equals("partially decayed")) {
                    n = 1;
                    break;
                }
                break;
            }
            case -1499650712: {
                if (lowerCase.equals("highly decayed")) {
                    n = 2;
                    break;
                }
                break;
            }
            case 375136829: {
                if (lowerCase.equals("heavily decayed")) {
                    n = 3;
                    break;
                }
                break;
            }
            case 1921722431: {
                if (lowerCase.equals("completely decayed")) {
                    n = 4;
                    break;
                }
                break;
            }
        }
        ITextComponent decayStatusFmt = null;
        switch (n) {
            case 0: {
                decayStatusFmt = (ITextComponent) new TextComponentString("");
                break;
            }
            case 1: {
                decayStatusFmt = (ITextComponent) new TextComponentString(MessageFormat.format("  {0}partially decayed", this.gray));
                break;
            }
            case 2: {
                decayStatusFmt = (ITextComponent) new TextComponentString(MessageFormat.format("  {0}highly decayed", this.gold));
                break;
            }
            case 3: {
                decayStatusFmt = (ITextComponent) new TextComponentString(MessageFormat.format("  {0}heavily decayed", this.red));
                break;
            }
            case 4: {
                decayStatusFmt = (ITextComponent) new TextComponentString(MessageFormat.format("  {0}completely decayed", this.darkRed));
                break;
            }
            default: {
                ChatHandler.log.error(String.format("%s%s: Unknown decay status. Status: '%s'.", this.getLogPrefixERROR(), selfName, decayStatus));
                return true;
            }
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixCT).appendSibling(groupNameFmt).appendSibling((ITextComponent) new TextComponentString("  ")).appendSibling(materialNameFmt).appendSibling((ITextComponent) new TextComponentString("  ")).appendSibling(remainingBlockBreakCountFmt).appendSibling((ITextComponent) new TextComponentString("  ")).appendSibling(strengthFmt).appendSibling(decayStatusFmt).appendSibling(hardeningStatusFmt).appendSibling((ITextComponent) new TextComponentString(this.reset + " "));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private ITextComponent getFormattedReinforcementMaterial(final String materialName) throws IllegalArgumentException {
        int n = -1;
        switch (materialName.hashCode()) {
            case 80218181: {
                if (materialName.equals("Stone")) {
                    n = 0;
                    break;
                }
                break;
            }
            case -1345457169: {
                if (materialName.equals("Iron Ingot")) {
                    n = 1;
                    break;
                }
                break;
            }
            case 30590468: {
                if (materialName.equals("Emerald")) {
                    n = 2;
                    break;
                }
                break;
            }
            case -975259340: {
                if (materialName.equals("Diamond")) {
                    n = 3;
                    break;
                }
                break;
            }
        }
        ITextComponent materialFmt = null;
        switch (n) {
            case 0: {
                final HoverEvent hover = new HoverEvent(HoverEvent.Action.SHOW_TEXT, (ITextComponent) new TextComponentString("Durability: 100  Maturation: 1 day"));
                materialFmt = new TextComponentString(MessageFormat.format("{0}Stone", this.white)).setStyle(new Style().setHoverEvent(hover));
                break;
            }
            case 1: {
                final HoverEvent hover = new HoverEvent(HoverEvent.Action.SHOW_TEXT, (ITextComponent) new TextComponentString("Durability: 500  Maturation: 12 hours"));
                materialFmt = new TextComponentString(MessageFormat.format("{0}Iron", this.white)).setStyle(new Style().setHoverEvent(hover));
                break;
            }
            case 2: {
                final HoverEvent hover = new HoverEvent(HoverEvent.Action.SHOW_TEXT, (ITextComponent) new TextComponentString("Durability: 1000  Maturation: 6 hours"));
                materialFmt = new TextComponentString(MessageFormat.format("{0}Emerald", this.white)).setStyle(new Style().setHoverEvent(hover));
                break;
            }
            case 3: {
                final HoverEvent hover = new HoverEvent(HoverEvent.Action.SHOW_TEXT, (ITextComponent) new TextComponentString("Durability: 1500  Maturation: 3 hours"));
                materialFmt = new TextComponentString(MessageFormat.format("{0}Diamond", this.white)).setStyle(new Style().setHoverEvent(hover));
                break;
            }
            default: {
                throw new IllegalArgumentException();
            }
        }
        return materialFmt;
    }

    private int getReinforcementMaterialStrengthBreakCount(final String materialName) throws IllegalArgumentException {
        int n = -1;
        switch (materialName.hashCode()) {
            case 80218181: {
                if (materialName.equals("Stone")) {
                    n = 0;
                    break;
                }
                break;
            }
            case -1345457169: {
                if (materialName.equals("Iron Ingot")) {
                    n = 1;
                    break;
                }
                break;
            }
            case 30590468: {
                if (materialName.equals("Emerald")) {
                    n = 2;
                    break;
                }
                break;
            }
            case -975259340: {
                if (materialName.equals("Diamond")) {
                    n = 3;
                    break;
                }
                break;
            }
        }
        switch (n) {
            case 0: {
                return 100;
            }
            case 1: {
                return 500;
            }
            case 2: {
                return 1000;
            }
            case 3: {
                return 1500;
            }
            default: {
                throw new IllegalArgumentException();
            }
        }
    }

    private String mcChatLeftPad(final String str, final int size, final String padStr) {
        final int delta = size - str.length();
        if (delta <= 0) {
            return str;
        }
        return StringUtils.repeat(padStr, delta) + str;
    }

    private boolean onCtiOff(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*REINFORCEMENT_INFORMATION (?:has been disabled|mode off)\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixCT).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}/cti  {1}OFF{2} ", this.gray, this.red, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onReinforcementGroupPermissionError(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("^(?i)^\\s*You do not have permission to place a reinforcement on this group\\.?\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.matches()) {
            return false;
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixCT).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Error:  {1}No permission to reinforce to that group.{2} ", this.red, this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onCtfOnFirst(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*You are now in Fortification mode, place blocks down and they will be secured with the material in your hand\\.?\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.matches()) {
            return false;
        }
        ITextComponent materialNameFmt;
        try {
            materialNameFmt = this.getHeldReinforcementMaterialNameFmt();
        } catch (Exception err) {
            return true;
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixCT).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}/ctf  {1}ON", this.gray, this.green))).appendSibling(materialNameFmt).appendSibling((ITextComponent) new TextComponentString(" " + this.reset));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private ITextComponent getHeldReinforcementMaterialNameFmt() throws Exception {

        final String selfName = "ChatFilter";
        final ItemStack heldItemStack = Minecraft.getMinecraft().player.getHeldItemMainhand();
        String materialName = null;
        if (heldItemStack != null) {
            final Item heldItem = heldItemStack.getItem();
            final ItemStack itemStackStone = new ItemStack(Blocks.STONE);
            final Item itemStone = itemStackStone.getItem();
            if (heldItem == itemStone) {
                materialName = "Stone";
            } else if (heldItem == Items.IRON_INGOT) {
                materialName = "Iron Ingot";
            } else if (heldItem == Items.EMERALD) {
                materialName = "Emerald";
            } else if (heldItem == Items.DIAMOND) {
                materialName = "Diamond";
            }
        }
        ITextComponent materialNameFmt;
        if (materialName == null) {
            materialNameFmt = (ITextComponent) new TextComponentString("");
        } else {
            try {
                materialNameFmt = new TextComponentString("  ").appendSibling(this.getFormattedReinforcementMaterial(materialName));
            } catch (IllegalArgumentException err) {
                ChatHandler.log.error(String.format("%s%s: This should never happen.", this.getLogPrefixERROR(), selfName));
                throw new Exception();
            }
        }
        return materialNameFmt;
    }

    private boolean onCtfOnSecond(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*Type /fortify or /cto to turn this off when you are done\\.?\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        event.setCanceled(true);
        return true;
    }

    private boolean onCtfOff(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*REINFORCEMENT_FORTIFICATION (has been disabled|mode off)\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixCT).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}/ctf  {1}OFF{2} ", this.gray, this.red, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onCtfOffMaterialDepleted(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*(.+?) depleted, left fortification mode\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.matches()) {
            return false;
        }
        if (match.group(1) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final String rawBlockName = match.group(1);
        final String blockName = this.rawBlockNameToHumanReadable(rawBlockName);
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixCT).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Error:  {1}{2} {3}depleted.{4} ", this.red, this.white, blockName, this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixCT).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}/ctf  {1}OFF{2} ", this.gray, this.red, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onCtfEmptyHandError(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*You need to be holding something to fortify with, try holding a stone block in your hand\\.?\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.matches()) {
            return false;
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixCT).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Error:  {1}No material selected.{2} ", this.red, this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onCtfWrongMaterialError(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*You can't use the item in your hand to reinforce\\.? Try using a stone block\\.?\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.matches()) {
            return false;
        }
        final String heldItemName = this.getHeldItemName();
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        if (heldItemName == null || this.isReinforcementName(heldItemName)) {
            msg = msg.appendSibling(this.prefixCT).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Error:  {1}Cannot reinforce with that block.{2} ", this.red, this.gray, this.reset)));
        } else {
            msg = msg.appendSibling(this.prefixCT).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Error:  {1}Cannot reinforce with {2}{3}{4}.{5} ", this.red, this.gray, this.white, heldItemName, this.gray, this.reset)));
        }
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private String getHeldItemName() {

        final String selfName = "ChatFilter";
        final ItemStack heldItemStack = Minecraft.getMinecraft().player.getHeldItemMainhand();
        if (heldItemStack != null) {
            return heldItemStack.getDisplayName();
        }
        return null;
    }

    private Boolean isReinforcementName(final String itemName) {
        final List<String> names = Arrays.<String>asList("Stone", "Iron", "Iron Ingot", "Emerald", "Diamond");
        for (final String name : names) {
            if (name.toLowerCase().equals(itemName.toLowerCase())) {
                return Boolean.valueOf(true);
            }
        }
        return Boolean.valueOf(false);
    }

    private boolean onReinforceGroupDoesntExistError(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*That group does not exist\\.?$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.matches()) {
            return false;
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixCT).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Error:  {1}That group doesn''t exist.{2} ", this.red, this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onReinfoceWrongMaterialBlockCombinationError(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*That material cannot reinforce that type of block\\.? Try a different reinforcement material\\.?\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.matches()) {
            return false;
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixCT).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Error:  {1}Cannot reinforce that block with the selected reinforcement material.{2} ", this.red, this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onCtrOnFirst(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*You are now in Reinforcement mode, hit blocks with a reinforcement material to secure them\\.?\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.matches()) {
            return false;
        }
        ITextComponent materialNameFmt;
        try {
            materialNameFmt = this.getHeldReinforcementMaterialNameFmt();
        } catch (Exception err) {
            return true;
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixCT).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}/ctr  {1}ON", this.gray, this.green))).appendSibling(materialNameFmt).appendSibling((ITextComponent) new TextComponentString(" " + this.reset));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onCtrOnSecond(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*Type /reinforce or /cto to turn this off when you are done\\.?\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        event.setCanceled(true);
        return true;
    }

    private boolean onCtrOff(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*(reinforcement has been disabled|left reinforcement mode\\.?|reinforcement mode off)\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixCT).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}/ctr  {1}OFF{2} ", this.gray, this.red, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onCtrGroupChange(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("^\\s*Group has been changed to: (.+?)\\.?\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.matches()) {
            return false;
        }
        if (match.group(1) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final String groupName = match.group(1);
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixCT).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}New group:  {1}[{2}]{3} ", this.gray, this.gold, groupName, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onCtrMaterialChange(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("^\\s*Changed reinforcement type\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.matches()) {
            return false;
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixCT).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Changed the reinforcement material.{1} ", this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onCtrUnreinforceableBlockError(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*That block cannot be reinforced\\.?\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.matches()) {
            return false;
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixCT).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Error:  {1}That block cannot be reinforced.{2} ", this.red, this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onCtrNoPermissionError(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*you are not permitted to modify this reinforcement\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.matches()) {
            return false;
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixCT).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Error:  {1}No permission to change the group.{2} ", this.red, this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onCtrWrongMaterialError(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*(.+?) is not a reinforcable material\\.?\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.matches()) {
            return false;
        }
        if (match.group(1) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final String rawBlockName = match.group(1);
        final String blockName = this.rawBlockNameToHumanReadable(rawBlockName);
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixCT).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Error:  {1}Cannot reinforce with {2}{3}{4}.{5} ", this.red, this.gray, this.white, blockName, this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onCtbOn(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*bypass mode has been enabled");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixCT).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}/ctb  {1}ON{2} ", this.gray, this.green, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onCtbOff(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*bypass mode has been disabled");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixCT).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}/ctb  {1}OFF{2} ", this.gray, this.red, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onCtbOffWarning(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*Enable bypass mode with");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixCT).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Protip:  Type {1}/ctb {2}to easily break this reinforced block.{3} ", this.gray, this.white, this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onCteOn(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*Easy mode has been enabled\\.?\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixCT).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}/cte  {1}ON{2} ", this.gray, this.green, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onCteOff(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*Easy mode has been disabled\\.?\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixCT).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}/cte  {1}OFF{2} ", this.gray, this.red, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onSnitchNotification(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("^\\s*\\* (.+?) (.+?) snitch at ?(.+?)? \\[([-\\w]+?) (-?[0-9]+) (-?[0-9]+) (-?[0-9]+)\\]\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.matches()) {
            return false;
        }
        if (match.group(1) == null || match.group(2) == null || match.group(4) == null || match.group(5) == null || match.group(6) == null || match.group(7) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final String playerName = match.group(1);
        final String eventType = match.group(2);
        final String rawName = match.group(3);
        final String dimensionNameRaw = match.group(4);
        final double coordX = Double.parseDouble(match.group(5));
        final double coordY = Double.parseDouble(match.group(6));
        final double coordZ = Double.parseDouble(match.group(7));
        final double playerCoordX = Minecraft.getMinecraft().player.posX;
        final double playerCoordY = Minecraft.getMinecraft().player.posY;
        final double playerCoordZ = Minecraft.getMinecraft().player.posZ;
        final double distance = this.getDistance(playerCoordX, playerCoordY, playerCoordZ, coordX, coordY, coordZ);
        TextFormatting distanceColor;
        if (distance <= 100.0) {
            distanceColor = this.red;
        } else if (distance <= 300.0) {
            distanceColor = this.gold;
        } else if (distance <= 500.0) {
            distanceColor = this.white;
        } else {
            distanceColor = this.gray;
        }
        ITextComponent distanceFmt = (ITextComponent) new TextComponentString(MessageFormat.format("{0}{1}m", distanceColor, this.mcChatLeftPad(String.valueOf(Math.round(distance)), 5, "  ")));
        final double yaw = this.getYaw(playerCoordX, playerCoordZ, coordX, coordZ);
        ChatHandler.log.debug(String.format("%syaw = %f = getYaw(x1:%f, z1:%f, x2:%f, z2:%f).", this.getLogPrefixDEBUG(), Double.valueOf(yaw), Double.valueOf(playerCoordX), Double.valueOf(playerCoordZ), Double.valueOf(coordX), Double.valueOf(coordZ)));
        if (yaw < 0.0 || yaw > 360.0) {
            ChatHandler.log.error(String.format("%s%s: Yaw out of range for yaw = %f = getYaw(x1:%f, z1:%f, x2:%f, z2:%f).", this.getLogPrefixERROR(), selfName, Double.valueOf(yaw), Double.valueOf(playerCoordX), Double.valueOf(playerCoordZ), Double.valueOf(coordX), Double.valueOf(coordZ)));
            return true;
        }
        String direction;
        if (yaw >= 354.37 || yaw < 11.25) {
            direction = "N";
        } else if (yaw >= 11.25 && yaw < 33.75) {
            direction = "NNE";
        } else if (yaw >= 33.75 && yaw < 56.25) {
            direction = "NE";
        } else if (yaw >= 56.25 && yaw < 78.75) {
            direction = "ENE";
        } else if (yaw >= 78.75 && yaw < 101.25) {
            direction = "E";
        } else if (yaw >= 101.25 && yaw < 123.75) {
            direction = "ESE";
        } else if (yaw >= 123.75 && yaw < 146.25) {
            direction = "SE";
        } else if (yaw >= 146.25 && yaw < 168.75) {
            direction = "SSE";
        } else if (yaw >= 168.75 && yaw < 191.25) {
            direction = "S";
        } else if (yaw >= 191.25 && yaw < 213.75) {
            direction = "SSW";
        } else if (yaw >= 213.75 && yaw < 236.25) {
            direction = "SW";
        } else if (yaw >= 236.25 && yaw < 258.75) {
            direction = "WSW";
        } else if (yaw >= 258.75 && yaw < 281.25) {
            direction = "W";
        } else if (yaw >= 281.25 && yaw < 303.75) {
            direction = "WNW";
        } else if (yaw >= 303.75 && yaw < 326.25) {
            direction = "NW";
        } else {
            if (yaw < 326.25 || yaw >= 354.37) {
                ChatHandler.log.error(String.format("%s%s: Broken yaw -> direction if chain for yaw = %f = getYaw(x1:%f, z1:%f, x2:%f, z2:%f).", this.getLogPrefixERROR(), selfName, Double.valueOf(yaw), Double.valueOf(playerCoordX), Double.valueOf(playerCoordZ), Double.valueOf(coordX), Double.valueOf(coordZ)));
                return true;
            }
            direction = "NNW";
        }
        ITextComponent directionWhitespaceFmt = (ITextComponent) new TextComponentString("    ");
        ITextComponent directionFmt = (ITextComponent) new TextComponentString(MessageFormat.format("{0}{1}", distanceColor, this.mcChatRightPad(direction, 3, "  ")));
        ITextComponent eventTypeFmt;
        if (eventType.toLowerCase().equals("entered")) {
            eventTypeFmt = (ITextComponent) new TextComponentString("");
        } else if (eventType.toLowerCase().equals("logged in to")) {
            eventTypeFmt = (ITextComponent) new TextComponentString(MessageFormat.format("    {0}Login", this.white));
        } else {
            if (!eventType.toLowerCase().equals("logged out in")) {
                ChatHandler.log.error(String.format("%s%s: Unrecognized snitch event type. Type: '%s'.", this.getLogPrefixERROR(), selfName, eventType));
                return true;
            }
            eventTypeFmt = (ITextComponent) new TextComponentString(MessageFormat.format("    {0}Logout", this.white));
        }
        final String lowerCase = dimensionNameRaw.trim().toLowerCase();
        int n = -1;
        switch (lowerCase.hashCode()) {
            case 113318802: {
                if (lowerCase.equals("world")) {
                    n = 0;
                    break;
                }
                break;
            }
            case 1865466277: {
                if (lowerCase.equals("world_nether")) {
                    n = 1;
                    break;
                }
                break;
            }
            case -1198266272: {
                if (lowerCase.equals("world_the_end")) {
                    n = 2;
                    break;
                }
                break;
            }
        }
        String dimensionName = null;
        int dimensionId = 0;
        switch (n) {
            case 0: {
                dimensionName = "World";
                dimensionId = 0;
                break;
            }
            case 1: {
                dimensionName = "Nether";
                dimensionId = -1;
                break;
            }
            case 2: {
                dimensionName = "The End";
                dimensionId = 1;
                break;
            }
            default: {
                ChatHandler.log.error(String.format("%s%s: Unrecognized snitch dimension. Dimension: '%s'.", this.getLogPrefixERROR(), selfName, dimensionNameRaw));
                return true;
            }
        }
        final int playerDimensionId = Minecraft.getMinecraft().world.provider.getDimension();
        String playerDimensionName = null;
        switch (playerDimensionId) {
            case 0: {
                playerDimensionName = "World";
                break;
            }
            case -1: {
                playerDimensionName = "Nether";
                break;
            }
            case 1: {
                playerDimensionName = "The End";
                break;
            }
            default: {
                ChatHandler.log.error(String.format("%s%s: Unrecognized player dimension name ID. ID: '%d'.", this.getLogPrefixERROR(), selfName, Integer.valueOf(playerDimensionId)));
                return true;
            }
        }
        ITextComponent dimensionNameFmt;
        if (dimensionName.equals(playerDimensionName)) {
            dimensionNameFmt = (ITextComponent) new TextComponentString("");
        } else {
            dimensionNameFmt = (ITextComponent) new TextComponentString(MessageFormat.format("{0}{1}", this.white, dimensionName));
            distanceFmt = (ITextComponent) new TextComponentString("");
            directionFmt = (ITextComponent) new TextComponentString("");
            directionWhitespaceFmt = (ITextComponent) new TextComponentString("");
        }
        final ITextComponent playerNameFmt = this.getFormattedPlayerName(playerName);
        final String coords = MessageFormat.format("{0,number,#} {1,number,#} {2,number,#}", Double.valueOf(coordX), Double.valueOf(coordY), Double.valueOf(coordZ));
        final ITextComponent coordsWhitespaceFmt = (ITextComponent) new TextComponentString("    ");
        ITextComponent coordsFmt = (ITextComponent) new TextComponentString(MessageFormat.format("{0}[{1}]", this.gray, coords));
        final int obfuscatedCoordX = this.getObfuscatedCoordXZ(coordX);
        final int obfuscatedCoordY = this.getObfuscatedCoordY(coordY);
        final int obfuscatedCoordZ = this.getObfuscatedCoordXZ(coordZ);
        final String obfuscatedCoords = MessageFormat.format("{0,number,#} {1,number,#} {2,number,#}", Integer.valueOf(obfuscatedCoordX), Integer.valueOf(obfuscatedCoordY), Integer.valueOf(obfuscatedCoordZ));
        final String dimensionNameTmp = MessageFormat.format("{0} ", dimensionNameRaw.trim().toLowerCase());
        final ClickEvent suggestCommandCoords = new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, MessageFormat.format("{0} was last seen around [{1}{2}]", playerName, dimensionNameTmp, obfuscatedCoords));
        coordsFmt = coordsFmt.setStyle(new Style().setClickEvent(suggestCommandCoords));
        ITextComponent nameWhitespaceFmt;
        ITextComponent nameFmt;
        if (rawName == null || rawName.trim().equals("")) {
            nameWhitespaceFmt = (ITextComponent) new TextComponentString("  ");
            nameFmt = (ITextComponent) new TextComponentString("  ");
        } else {
            nameWhitespaceFmt = (ITextComponent) new TextComponentString("    ");
            nameFmt = this.getColoredSnitchName(rawName);
        }
        final ClickEvent runJalookup = new ClickEvent(ClickEvent.Action.RUN_COMMAND, MessageFormat.format("/jalookup {0} {1}", coords, dimensionNameRaw));
        nameFmt = nameFmt.setStyle(new Style().setClickEvent(runJalookup));
        final ITextComponent jmWaypointFmt = this.getClickableJmWaypointFmt(coordX, Double.valueOf(coordY), coordZ, rawName, Integer.valueOf(dimensionId));
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixSN).appendSibling(distanceFmt).appendSibling(directionWhitespaceFmt).appendSibling(directionFmt).appendSibling(dimensionNameFmt).appendSibling((ITextComponent) new TextComponentString("    ")).appendSibling(playerNameFmt).appendSibling(eventTypeFmt).appendSibling(nameWhitespaceFmt).appendSibling(nameFmt).appendSibling(coordsWhitespaceFmt).appendSibling(coordsFmt).appendSibling((ITextComponent) new TextComponentString("  ")).appendSibling(jmWaypointFmt).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0} ", this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private ITextComponent getClickableJmWaypointFmt(final double coordX, final Double coordY, final double coordZ, final String name, final Integer mcPlayerDimensionId) {

        final String selfName = "ChatFilter";
        String jmCommand = "/jm wpedit [";
        if (name != null) {
            jmCommand += MessageFormat.format("name:{0}, ", name);
        }
        jmCommand += MessageFormat.format("x:{0,number,#}, ", Double.valueOf(coordX));
        if (coordY != null) {
            jmCommand += MessageFormat.format("y:{0,number,#}, ", coordY);
        }
        jmCommand += MessageFormat.format("z:{0,number,#}, ", Double.valueOf(coordZ));
        if (mcPlayerDimensionId != null) {
            jmCommand += MessageFormat.format("dim:{0,number,#}, ", mcPlayerDimensionId);
        }
        jmCommand += "]";
        final ClickEvent runCommandJmWaypoint = new ClickEvent(ClickEvent.Action.RUN_COMMAND, jmCommand);
        final HoverEvent hoverJmWaypoint = new HoverEvent(HoverEvent.Action.SHOW_TEXT, (ITextComponent) new TextComponentString("Click to add as a JM waypoint. Ctrl+Click to show on the map."));
        final ITextComponent jmWaypointFmt = new TextComponentString(MessageFormat.format("{0}+{1}", this.gray, this.reset)).setStyle(new Style().setClickEvent(runCommandJmWaypoint).setHoverEvent(hoverJmWaypoint));
        return jmWaypointFmt;
    }

    private double getDistance(final double x1, final double y1, final double z1, final double x2, final double y2, final double z2) {
        final double deltaX = x2 - x1;
        final double deltaY = y2 - y1;
        final double deltaZ = z2 - z1;
        return Math.sqrt(deltaX * deltaX + deltaY * deltaY + deltaZ * deltaZ);
    }

    private double getYaw(final double x1, final double z1, final double x2, final double z2) {
        final double deltaZ = z2 - z1;
        final double deltaX = x2 - x1;
        final double yaw = Math.toDegrees(Math.atan2(deltaZ, deltaX) + 1.5707963267948966);
        if (yaw < 0.0) {
            return 360.0 + yaw;
        }
        return yaw;
    }

    private String mcChatRightPad(final String str, final int size, final String padStr) {
        final int delta = size - str.length();
        if (delta <= 0) {
            return str;
        }
        return str + StringUtils.repeat(padStr, delta);
    }

    private int getObfuscatedCoordXZ(final double coord) {

        final String selfName = "ChatFilter";
        boolean negativeSign = false;
        if (coord < 0.0) {
            negativeSign = true;
        }
        final Double coordAbsTmp = Double.valueOf(Math.abs(coord));
        final int coordAbs = coordAbsTmp.intValue();
        if (coordAbs < 25) {
            return 0;
        }
        if (coordAbs < 75) {
            if (negativeSign) {
                return -50;
            }
            return 50;
        } else if (coordAbs < 125) {
            if (negativeSign) {
                return -100;
            }
            return 100;
        } else {
            final String coordStr = String.valueOf(coordAbs);
            final String[] coordSplit = coordStr.split("");
            final String[] coordFirstPart = (String[]) Arrays.<String>copyOfRange(coordSplit, 0, coordStr.length() - 3);
            final int coordLastDigit = Integer.parseInt(coordSplit[coordStr.length() - 1]);
            final int coord2LastDigit = Integer.parseInt(coordSplit[coordStr.length() - 2]);
            final int coord3LastDigit = Integer.parseInt(coordSplit[coordStr.length() - 3]);
            final int last2number = Integer.parseInt(Arrays.toString(Arrays.<String>copyOfRange(coordSplit, coordStr.length() - 2, coordStr.length())).replaceAll("\\[|\\]|,|\\s", ""));
            int coordLastDigitdOut = 0;
            int coord2LastDigitOut = 0;
            int coord3LastDigitOut = 0;
            boolean addOneAtTheEnd = false;
            if (last2number < 25) {
                coordLastDigitdOut = 0;
                coord2LastDigitOut = 0;
                coord3LastDigitOut = coord3LastDigit;
            } else if (last2number < 75) {
                coordLastDigitdOut = 0;
                coord2LastDigitOut = 5;
                coord3LastDigitOut = coord3LastDigit;
            } else if (last2number < 100) {
                if (coord3LastDigit != 9) {
                    coordLastDigitdOut = 0;
                    coord2LastDigitOut = 0;
                    coord3LastDigitOut = coord3LastDigit + 1;
                } else {
                    addOneAtTheEnd = true;
                    coordLastDigitdOut = 9;
                    coord2LastDigitOut = 9;
                    coord3LastDigitOut = 9;
                }
            }
            final String coordStrOutAbs = Arrays.toString(coordFirstPart).replaceAll("\\[|\\]|,|\\s", "") + String.valueOf(coord3LastDigitOut) + String.valueOf(coord2LastDigitOut) + String.valueOf(coordLastDigitdOut);
            int coordOutAbs = Integer.parseInt(coordStrOutAbs);
            if (addOneAtTheEnd) {
                ++coordOutAbs;
            }
            if (negativeSign) {
                return -1 * coordOutAbs;
            }
            return coordOutAbs;
        }
    }

    private int getObfuscatedCoordY(final double coord) {

        final String selfName = "ChatFilter";
        if (coord < 0.0) {
            ChatHandler.log.warn(String.format("[Chatfilters] %s: Y coord below 0: '%f'.", selfName, Double.valueOf(coord)));
            return 0;
        }
        if (coord < 15.0) {
            return 0;
        }
        if (coord < 45.0) {
            return 30;
        }
        if (coord < 75.0) {
            return 60;
        }
        if (coord < 105.0) {
            return 90;
        }
        if (coord < 135.0) {
            return 120;
        }
        if (coord < 165.0) {
            return 150;
        }
        if (coord < 195.0) {
            return 180;
        }
        if (coord < 225.0) {
            return 210;
        }
        if (coord < 256.0) {
            return 240;
        }
        ChatHandler.log.warn(String.format("[Chatfilters] %s: Y coord above 255: '%f'.", selfName, Double.valueOf(coord)));
        return 255;
    }

    private ITextComponent getColoredSnitchName(final String name) {

        final String selfName = "ChatFilter";
        if (name.contains("!")) {
            return (ITextComponent) new TextComponentString(MessageFormat.format("{0}{1}", this.red, name));
        }
        ITextComponent nameFmt = (ITextComponent) new TextComponentString(MessageFormat.format("{0}{1}", this.gray, name));
        final Pattern perimRegex = Pattern.compile("(?i)(Perim\\d+)");
        final Matcher perimMatch = perimRegex.matcher(name);
        if (perimMatch.find()) {
            final String perimName = perimMatch.group(1);
            final String[] nameSplit = name.split("(?i)Perim\\d+");
            String namePrefix = "";
            String nameSuffix = "";
            if (nameSplit.length == 1) {
                namePrefix = nameSplit[0];
            } else if (nameSplit.length == 2) {
                namePrefix = nameSplit[0];
                nameSuffix = nameSplit[1];
            } else if (nameSplit.length > 2) {
                ChatHandler.log.warn(String.format("%s%s: More than one 'Perim' in snitch name '%s'.", this.getLogPrefixWARNING(), selfName, name));
                return nameFmt;
            }
            nameFmt = (ITextComponent) new TextComponentString(MessageFormat.format("{0}{1}{2}{3}{4}{5}", this.gray, namePrefix, this.white, perimName, this.gray, nameSuffix));
        }
        return nameFmt;
    }

    private boolean onUnreinforcedSnitchPlace(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*you've placed a (noteblock|jukebox); reinforce it to register it as (?:an entry|a) snitch\\.?\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.matches()) {
            return false;
        }
        if (match.group(1) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final String type = match.group(1);
        String blockName;
        if (type.toLowerCase().equals("noteblock")) {
            blockName = "Noteblock";
        } else {
            if (!type.toLowerCase().equals("jukebox")) {
                ChatHandler.log.error(String.format("%s%s: Unrecognized snitch type. Type: '%s'.", this.getLogPrefixERROR(), selfName, type));
                return true;
            }
            blockName = "Jukebox";
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixJA).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Reinforce the {1}{2} {3}to turn it into a snitch.{4} ", this.gray, this.white, blockName, this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onSnitchPlace(final ClientChatReceivedEvent event, final String message, final String messageClean, final ITextComponent rawMessage) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("^\\s*You've created (an entry snitch|a snitch) registered to the group (.+?)\\.?\\s*To name it, type \\/janame\\.?\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.matches()) {
            return false;
        }
        if (match.group(1) == null || match.group(2) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final String type = match.group(1);
        final String groupName = match.group(2);
        String typeName;
        if (type.toLowerCase().equals("an entry snitch")) {
            typeName = "entry";
        } else {
            if (!type.toLowerCase().equals("a snitch")) {
                ChatHandler.log.error(String.format("%s%s: Unrecognized snitch type. Type: '%s'.", this.getLogPrefixERROR(), selfName, type));
                return true;
            }
            typeName = "logging";
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        final Snitch snitch = this.getParsedSnitchHoverInfo(rawMessage);
        if (snitch == null || snitch.coords == null) {
            msg = msg.appendSibling(this.prefixJA).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}New {1}{2} {3}snitch on {4}[{5}]{6} ", this.gray, this.white, typeName, this.gray, this.gold, groupName, this.reset)));
        } else {
            msg = msg.appendSibling(this.prefixJA).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}New {1}{2} {3}snitch at [{4}] on {5}[{6}]{7} ", this.gray, this.white, typeName, this.gray, snitch.coords, this.gold, groupName, this.reset)));
        }
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private Snitch getParsedSnitchHoverInfo(final ITextComponent rawMessage) {
        final List<ITextComponent> siblings = (List<ITextComponent>) rawMessage.getSiblings();
        if (siblings.size() <= 0) {
            return null;
        }
        final ITextComponent hoverComponent = (ITextComponent) siblings.get(0);
        final HoverEvent hover = hoverComponent.getStyle().getHoverEvent();
        if (hover == null) {
            return null;
        }
        final String rawText = hover.getValue().getUnformattedComponentText().replace("\n", " ");
        final Pattern regex = Pattern.compile("^(?i)\\s*(?:World: (.+?)(?:\\s|$))?\\s*(?:Location: \\[(-?[0-9]+) (-?[0-9]+) (-?[0-9]+)\\])?\\s*(?:Group: (.+?)(?:\\s|$))?\\s*(?:Hours to cull: ([0-9]+\\.[0-9]+)h?)?\\s*(?:Previous name: (.+?)(?:\\s|$))?\\s*(?:(?:New )?Name: (.+?)(?:\\s|$))?\\s*");
        final Matcher match = regex.matcher(rawText);
        if (!match.matches()) {
            return null;
        }
        final Snitch snitch = new Snitch();
        snitch.worldName = match.group(1);
        if (match.group(2) == null || match.group(2).isEmpty()) {
            snitch.coordX = Double.valueOf(Double.NaN);
        } else {
            snitch.coordX = Double.valueOf(Double.parseDouble(match.group(2)));
        }
        if (match.group(3) == null || match.group(3).isEmpty()) {
            snitch.coordY = Double.valueOf(Double.NaN);
        } else {
            snitch.coordY = Double.valueOf(Double.parseDouble(match.group(3)));
        }
        if (match.group(4) == null || match.group(4).isEmpty()) {
            snitch.coordZ = Double.valueOf(Double.NaN);
        } else {
            snitch.coordZ = Double.valueOf(Double.parseDouble(match.group(4)));
        }
        if (!snitch.coordX.isNaN() && !snitch.coordY.isNaN() && !snitch.coordZ.isNaN()) {
            snitch.coords = MessageFormat.format("{0,number,#} {1,number,#} {2,number,#}", snitch.coordX, snitch.coordY, snitch.coordZ);
        } else {
            snitch.coords = null;
        }
        snitch.groupName = match.group(5);
        if (match.group(6) == null || match.group(6).isEmpty()) {
            snitch.cullTime = Double.valueOf(Double.NaN);
        } else {
            snitch.cullTime = Double.valueOf(Double.parseDouble(match.group(6)));
        }
        if (match.group(7) == null) {
            snitch.previousName = null;
        } else {
            snitch.previousName = match.group(7).trim();
        }
        if (match.group(8) == null) {
            snitch.name = null;
        } else {
            snitch.name = match.group(8).trim();
        }
        return snitch;
    }

    private boolean onJaname(final ClientChatReceivedEvent event, final String message, final String messageClean, final ITextComponent rawMessage) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("^\\s*Changed snitch name to (.+?)\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.matches()) {
            return false;
        }
        if (match.group(1) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final String name = match.group(1);
        final ITextComponent snitchNameFmt = this.getFormattedSnitchName(name);
        final Snitch snitch = this.getParsedSnitchHoverInfo(rawMessage);
        if (snitch == null || snitch.coords == null || snitch.previousName == null || snitch.groupName == null) {
            final ITextComponent msg = new TextComponentString("").appendSibling(this.prefixJA).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}The snitch got renamed to  ", this.gray))).appendSibling(snitchNameFmt).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0} ", this.reset)));
            Minecraft.getMinecraft().player.sendMessage(msg);
        } else if (name.equals(snitch.previousName)) {
            final ITextComponent msg = new TextComponentString("").appendSibling(this.prefixJA).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Error:  {1}The snitch at [{2}] on {3}[{4}] {5}is already named ", this.red, this.gray, snitch.coords, this.gold, snitch.groupName, this.gray))).appendSibling(snitchNameFmt).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0} ", this.reset)));
            Minecraft.getMinecraft().player.sendMessage(msg);
        } else {
            ITextComponent msg = new TextComponentString("").appendSibling(this.prefixJA).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}The snitch at [{1}] on {2}[{3}] {4}got renamed from/to:{5} ", this.gray, snitch.coords, this.gold, snitch.groupName, this.gray, this.reset)));
            Minecraft.getMinecraft().player.sendMessage(msg);
            if (snitch.previousName.isEmpty()) {
                msg = new TextComponentString("").appendSibling(this.prefixJA).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("  {0}unnamed{1} ", this.red, this.reset)));
                Minecraft.getMinecraft().player.sendMessage(msg);
            } else {
                final ITextComponent snitchPreviousNameFmt = this.getFormattedSnitchName(snitch.previousName);
                msg = new TextComponentString("").appendSibling(this.prefixJA).appendSibling((ITextComponent) new TextComponentString("  ")).appendSibling(snitchPreviousNameFmt).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0} ", this.reset)));
                Minecraft.getMinecraft().player.sendMessage(msg);
            }
            msg = new TextComponentString("").appendSibling(this.prefixJA).appendSibling((ITextComponent) new TextComponentString("  ")).appendSibling(snitchNameFmt).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0} ", this.reset)));
            Minecraft.getMinecraft().player.sendMessage(msg);
        }
        event.setCanceled(true);
        return true;
    }

    private ITextComponent getFormattedSnitchName(final String snitchName) {
        final ClickEvent suggestCommandJaname = new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, MessageFormat.format("/janame {0}", snitchName));
        final ITextComponent snitchNameFmt = new TextComponentString(MessageFormat.format("{0}{1}", this.white, snitchName)).setStyle(new Style().setClickEvent(suggestCommandJaname));
        return snitchNameFmt.appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("  {0}[{1}/{2}]{3}", this.darkGray, Integer.valueOf(snitchName.length()), Integer.valueOf(this.MAX_SNITCH_NAME_LENGTH), this.reset)));
    }

    private boolean onJalookupPermissionError(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*You don't have permission to lookup the group of this snitch\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixJA).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Error:  {1}No permission to /jalookup that snitch.{2} ", this.red, this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onJalookup(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("^\\s*The snitch at \\[(-?[0-9]+) (-?[0-9]+) (-?[0-9]+)\\] is owned by (.+?)\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.matches()) {
            return false;
        }
        if (match.group(1) == null || match.group(2) == null || match.group(3) == null || match.group(4) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final double coordX = Double.parseDouble(match.group(1));
        final double coordY = Double.parseDouble(match.group(2));
        final double coordZ = Double.parseDouble(match.group(3));
        final String groupName = match.group(4);
        final String coords = MessageFormat.format("{0,number,#} {1,number,#} {2,number,#}", Double.valueOf(coordX), Double.valueOf(coordY), Double.valueOf(coordZ));
        final ClickEvent runCommandJamute = new ClickEvent(ClickEvent.Action.RUN_COMMAND, MessageFormat.format("/jamute {0}", groupName));
        final ITextComponent runCommandJamuteFmt = new TextComponentString(MessageFormat.format("{0}[{1}]", this.gold, groupName)).setStyle(new Style().setClickEvent(runCommandJamute));
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixJA).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}The snitch at [{1}] is reinforced to ", this.gray, coords))).appendSibling(runCommandJamuteFmt).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0} ", this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onJaclear(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*Cleared all snitch logs\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixJA).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Snitch logs cleared.{1} ", this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onJainfoNoSnitchesError(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*You do not own any snitches nearby or lack permission to view their logs!\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixJA).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Error:  {1}No owned snitches within an 11 block radius.{2} ", this.red, this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onJainfoEntrySnitch(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("^\\s*\\*\\s*(Entry snitch (.+?)|Unnamed entry snitch)\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.matches()) {
            return false;
        }
        if (match.group(1) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final String type = match.group(1);
        final String name = match.group(2);
        ITextComponent nameFmt;
        if (name == null || type.trim().toLowerCase().equals("Unnamed entry snitch")) {
            nameFmt = (ITextComponent) new TextComponentString(MessageFormat.format("{0}Unnamed {1}entry snitch.", this.red, this.gray));
        } else {
            final ITextComponent snitchNameFmt = this.getFormattedSnitchName(name);
            nameFmt = new TextComponentString(MessageFormat.format("{0}Entry snitch ", this.gray)).appendSibling(snitchNameFmt);
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixJA).appendSibling(nameFmt).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0} ", this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onJainfoNoLogs(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("^\\s*\\*\\s*Page ([0-9]+) is empty for (snitch (.+?)|unnamed snitch)\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.matches()) {
            return false;
        }
        if (match.group(1) == null || match.group(2) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final int pageNumber = Integer.parseInt(match.group(1));
        final String type = match.group(2);
        final String name = match.group(3);
        ITextComponent nameFmt;
        if (name == null || type.trim().toLowerCase().equals("unnamed snitch")) {
            nameFmt = (ITextComponent) new TextComponentString(MessageFormat.format("{0}unnamed {1}logging snitch.", this.red, this.gray));
        } else {
            final ITextComponent snitchNameFmt = this.getFormattedSnitchName(name);
            nameFmt = new TextComponentString(MessageFormat.format("{0}logging snitch ", this.gray)).appendSibling(snitchNameFmt);
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        if (pageNumber == 1) {
            msg = msg.appendSibling(this.prefixJA).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}No logs for ", this.gray))).appendSibling(nameFmt).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0} ", this.reset)));
        } else {
            msg = msg.appendSibling(this.prefixJA).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}No logs for page {1}{2} {3}of ", this.gray, this.white, Integer.valueOf(pageNumber), this.gray))).appendSibling(nameFmt).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0} ", this.reset)));
        }
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onJainfoLogs(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("^\\s*Log for (snitch (.+?)|unnamed snitch)\\s*-*\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.matches()) {
            return false;
        }
        if (match.group(1) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final String type = match.group(1);
        final String name = match.group(2);
        ITextComponent nameFmt;
        if (name == null || type.trim().toLowerCase().equals("unnamed snitch")) {
            nameFmt = (ITextComponent) new TextComponentString(MessageFormat.format("{0}unnamed {1}logging snitch", this.red, this.gray));
        } else {
            final ITextComponent snitchNameFmt = this.getFormattedSnitchName(name);
            nameFmt = new TextComponentString(MessageFormat.format("{0}logging snitch ", this.gray)).appendSibling(snitchNameFmt);
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixJA).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Logs for ", this.gray))).appendSibling(nameFmt).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}:{0} ", this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onJainfoLogsPagination(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("^\\s*\\*\\s*Page ([0-9]+)\\s*-+\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        if (match.group(1) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final int currentPage = Integer.parseInt(match.group(1));
        final ITextComponent msg = this.getPagination(currentPage, null, "/jainfo");
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onJainfoKill(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("^\\s*(\\S+?)\\s*Killed\\s*(\\S+?)\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.matches()) {
            return false;
        }
        if (match.group(1) == null || match.group(2) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final String playerName = match.group(1);
        final String entityNameRaw = match.group(2);
        String entityName = entityNameRaw.replace("_", " ").toLowerCase();
        entityName = WordUtils.capitalize(entityName);
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixJA).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}{1}    {2}Killed    {3}{4}{5} ", this.darkAqua, this.mcChatRightPad(playerName, this.PLAYER_NAME_PADDING, "  "), this.red, this.white, entityName, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onJainfoEntryLoginLogout(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("^\\s*(\\S+?)\\s*(Entry|Login|Logout)\\s*([0-9]+-[0-9]+ [0-9]+:[0-9]+)\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.matches()) {
            return false;
        }
        if (match.group(1) == null || match.group(2) == null || match.group(3) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final String playerName = match.group(1);
        final String actionTypeRaw = match.group(2);
        String dateServerRaw = match.group(3);
        final DateFormat df = new SimpleDateFormat("yyyy");
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        final String CURRENT_YEAR = df.format(new Date());
        dateServerRaw = MessageFormat.format("{0}-{1}", CURRENT_YEAR, dateServerRaw);
        final DateFormat dateFormatServer = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date dateServer;
        try {
            dateServer = dateFormatServer.parse(dateServerRaw);
        } catch (ParseException err) {
            ChatHandler.log.error(String.format("%s%s: Failed to parse raw server date. Raw date: '%s'.", this.getLogPrefixERROR(), selfName, dateServerRaw));
            return true;
        }
        final Date dateUtc = DateUtils.addHours(dateServer, -this.SERVER_TIMEZONE_UTC_OFFSET);
        final String dateDiff = this.getDateDiff(new Date(), dateUtc);
        if (dateDiff == null) {
            ChatHandler.log.error(String.format("%s%s: Failed to get a date diff.", this.getLogPrefixERROR(), selfName));
            return true;
        }
        ITextComponent actionTypeFmt;
        if (actionTypeRaw.trim().toLowerCase().equals("entry")) {
            actionTypeFmt = (ITextComponent) new TextComponentString(MessageFormat.format("{0}Entered    ", this.white));
        } else if (actionTypeRaw.trim().toLowerCase().equals("login")) {
            actionTypeFmt = (ITextComponent) new TextComponentString(MessageFormat.format("{0}Logged in  ", this.lightPurple));
        } else {
            if (!actionTypeRaw.trim().toLowerCase().equals("logout")) {
                ChatHandler.log.error(String.format("%s%s: Unrecognized action type. Type: '%s'.", this.getLogPrefixERROR(), selfName, actionTypeRaw));
                return true;
            }
            actionTypeFmt = (ITextComponent) new TextComponentString(MessageFormat.format("{0}Logged out", this.lightPurple));
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixJA).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}{1}    ", this.darkAqua, this.mcChatRightPad(playerName, this.PLAYER_NAME_PADDING, "  ")))).appendSibling(actionTypeFmt).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("    {0}{1} ago    {2} {3}    {4} UTC {5} ", this.gray, dateDiff, new SimpleDateFormat("MM-dd HH:mm").format(dateServer), this.SERVER_TIMEZONE_NAME, new SimpleDateFormat("MM-dd HH:mm").format(dateUtc), this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private String getDateDiff(final Date date, final Date datePrev) {
        final long diffInMs = date.getTime() - datePrev.getTime();
        final List<TimeUnit> units = new ArrayList<TimeUnit>(EnumSet.<TimeUnit>allOf(TimeUnit.class));
        Collections.reverse(units);
        final Map<TimeUnit, Long> result = new LinkedHashMap<TimeUnit, Long>();
        long msRest = diffInMs;
        for (final TimeUnit unit : units) {
            final long diff = unit.convert(msRest, TimeUnit.MILLISECONDS);
            final long diffInMsForUnit = unit.toMillis(diff);
            msRest -= diffInMsForUnit;
            result.put(unit, Long.valueOf(diff));
        }
        if (Long.valueOf(result.get((Object) TimeUnit.DAYS)) == 0L && Long.valueOf(result.get((Object) TimeUnit.HOURS)) == 0L && Long.valueOf(result.get((Object) TimeUnit.MINUTES)) <= 0L) {
            return "<1m";
        }
        String out = "";
        if (Long.valueOf(result.get((Object) TimeUnit.DAYS)) != 0L) {
            out = MessageFormat.format("{0}{1}d", out, result.get(TimeUnit.DAYS));
        }
        if (Long.valueOf(result.get((Object) TimeUnit.HOURS)) != 0L) {
            out = MessageFormat.format("{0} {1}h", out, result.get(TimeUnit.HOURS));
        }
        if (Long.valueOf(result.get((Object) TimeUnit.MINUTES)) != 0L) {
            out = MessageFormat.format("{0} {1}m", out, result.get(TimeUnit.MINUTES));
        }
        return out;
    }

    private boolean onJainfoBlockUse(final ClientChatReceivedEvent event, final String message, final String messageClean) {
        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("^\\s*(\\S+?)\\s*Used\\s*([0-9]+)\\s*\\[(-?[0-9]+) (-?[0-9]+) (-?[0-9]+)\\]\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.matches()) {
            return false;
        }
        if (match.group(1) == null || match.group(2) == null || match.group(3) == null || match.group(4) == null || match.group(5) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final String playerName = match.group(1);
        final int blockId = Integer.parseInt(match.group(2));
        final double coordX = Double.parseDouble(match.group(3));
        final double coordY = Double.parseDouble(match.group(4));
        final double coordZ = Double.parseDouble(match.group(5));
        final String blockNameRaw = this.getBlockNameFromId(blockId);
        String blockName;
        if (blockNameRaw == null) {
            blockName = MessageFormat.format("ID-{0}", Integer.valueOf(blockId));
        } else {
            blockName = blockNameRaw;
        }
        final String coords = MessageFormat.format("{0,number,#} {1,number,#} {2,number,#}", Double.valueOf(coordX), Double.valueOf(coordY), Double.valueOf(coordZ));
        final String waypointName = MessageFormat.format("{0}_{1}_{2}", playerName, "Used", blockName);
        final int playerDimensionId = Minecraft.getMinecraft().world.provider.getDimension();
        final ITextComponent jmWaypointFmt = this.getClickableJmWaypointFmt(coordX, Double.valueOf(coordY), coordZ, waypointName, Integer.valueOf(playerDimensionId));
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixJA).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}{1}    {2}Used    {3}{4}    {5}[{6}]  ", this.darkAqua, this.mcChatRightPad(playerName, this.PLAYER_NAME_PADDING, "  "), this.yellow, this.white, blockName, this.gray, coords))).appendSibling(jmWaypointFmt).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0} ", this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onJainfoEntityUse(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("^\\s*(\\S+?)\\s*(Mount|Dismount|Destroyed)\\s*([0-9]+)\\s*\\[(-?[0-9]+) (-?[0-9]+) (-?[0-9]+)\\]\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.matches()) {
            return false;
        }
        if (match.group(1) == null || match.group(2) == null || match.group(3) == null || match.group(4) == null || match.group(5) == null || match.group(6) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final String playerName = match.group(1);
        final String actionTypeRaw = match.group(2);
        final int blockId = Integer.parseInt(match.group(3));
        final double coordX = Double.parseDouble(match.group(4));
        final double coordY = Double.parseDouble(match.group(5));
        final double coordZ = Double.parseDouble(match.group(6));
        String actionType;
        ITextComponent actionTypeFmt;
        if (actionTypeRaw.trim().toLowerCase().equals("mount")) {
            actionType = "Mounted";
            actionTypeFmt = (ITextComponent) new TextComponentString(MessageFormat.format("{0}Mounted       ", this.yellow));
        } else if (actionTypeRaw.trim().toLowerCase().equals("dismount")) {
            actionType = "Dismounted";
            actionTypeFmt = (ITextComponent) new TextComponentString(MessageFormat.format("{0}Dismounted  ", this.yellow));
        } else {
            if (!actionTypeRaw.trim().toLowerCase().equals("destroyed")) {
                ChatHandler.log.error(String.format("%s%s: Unrecognized action type. Type: '%s'.", this.getLogPrefixERROR(), selfName, actionTypeRaw));
                return true;
            }
            actionType = "Broke";
            actionTypeFmt = (ITextComponent) new TextComponentString(MessageFormat.format("{0}Broke           ", this.red));
        }
        final String blockNameRaw = this.getBlockNameFromId(blockId);
        String blockName;
        if (blockNameRaw == null) {
            blockName = MessageFormat.format("ID-{0}", Integer.valueOf(blockId));
        } else {
            blockName = blockNameRaw;
        }
        final String coords = MessageFormat.format("{0,number,#} {1,number,#} {2,number,#}", Double.valueOf(coordX), Double.valueOf(coordY), Double.valueOf(coordZ));
        final String waypointName = MessageFormat.format("{0}_{1}_{2}", playerName, actionType, blockName);
        final int playerDimensionId = Minecraft.getMinecraft().world.provider.getDimension();
        final ITextComponent jmWaypointFmt = this.getClickableJmWaypointFmt(coordX, Double.valueOf(coordY), coordZ, waypointName, Integer.valueOf(playerDimensionId));
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixJA).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}{1}    ", this.darkAqua, this.mcChatRightPad(playerName, this.PLAYER_NAME_PADDING, "  ")))).appendSibling(actionTypeFmt).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}{1}    {2}[{3}]  ", this.white, blockName, this.gray, coords))).appendSibling(jmWaypointFmt).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0} ", this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onJainfoBucketUse(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("^\\s*(\\S+?)\\s*Bucket (Fill|Empty)\\s*([0-9]+)\\s*\\[(-?[0-9]+) (-?[0-9]+) (-?[0-9]+)\\]\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.matches()) {
            return false;
        }
        if (match.group(1) == null || match.group(2) == null || match.group(3) == null || match.group(4) == null || match.group(5) == null || match.group(6) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final String playerName = match.group(1);
        final String changeTypeRaw = match.group(2);
        final int blockId = Integer.parseInt(match.group(3));
        final double coordX = Double.parseDouble(match.group(4));
        final double coordY = Double.parseDouble(match.group(5));
        final double coordZ = Double.parseDouble(match.group(6));
        String bucketType;
        String bucketName;
        if (blockId == 325) {
            bucketType = "";
            bucketName = "Bucket";
        } else if (blockId == 326) {
            bucketType = "Water";
            bucketName = "Water Bucket";
        } else {
            if (blockId != 327) {
                ChatHandler.log.error(String.format("%s%s: Unrecognized block id/bucket type. Block ID: '%d'.", this.getLogPrefixERROR(), selfName, Integer.valueOf(blockId)));
                return true;
            }
            bucketType = "Lava";
            bucketName = "Lava Bucket";
        }
        String changeType;
        if (changeTypeRaw.trim().toLowerCase().equals("fill")) {
            changeType = "Filled";
        } else {
            if (!changeTypeRaw.trim().toLowerCase().equals("empty")) {
                ChatHandler.log.error(String.format("%s%s: Unrecognized bucket change type. Type: '%s'.", this.getLogPrefixERROR(), selfName, changeTypeRaw));
                return true;
            }
            changeType = "Emptied";
        }
        final String coords = MessageFormat.format("{0,number,#} {1,number,#} {2,number,#}", Double.valueOf(coordX), Double.valueOf(coordY), Double.valueOf(coordZ));
        final String waypointName = MessageFormat.format("{0}_{1}_{2}Bucket", playerName, changeType, bucketType);
        final int playerDimensionId = Minecraft.getMinecraft().world.provider.getDimension();
        final ITextComponent jmWaypointFmt = this.getClickableJmWaypointFmt(coordX, Double.valueOf(coordY), coordZ, waypointName, Integer.valueOf(playerDimensionId));
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixJA).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}{1}    {2}{3} a {4}    {5}[{6}]  ", this.darkAqua, this.mcChatRightPad(playerName, this.PLAYER_NAME_PADDING, "  "), this.aqua, changeType, bucketName, this.gray, coords))).appendSibling(jmWaypointFmt).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0} ", this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onJainfoBlockChange(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("^\\s*(\\S+?)\\s*Block (Break|Place)\\s*([0-9]+)\\s*\\[(-?[0-9]+) (-?[0-9]+) (-?[0-9]+)\\]\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.matches()) {
            return false;
        }
        if (match.group(1) == null || match.group(2) == null || match.group(3) == null || match.group(4) == null || match.group(5) == null || match.group(6) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final String playerName = match.group(1);
        final String changeTypeRaw = match.group(2);
        final int blockId = Integer.parseInt(match.group(3));
        final double coordX = Double.parseDouble(match.group(4));
        final double coordY = Double.parseDouble(match.group(5));
        final double coordZ = Double.parseDouble(match.group(6));
        String changeType;
        ITextComponent changeTypeFmt;
        if (changeTypeRaw.trim().toLowerCase().equals("break")) {
            changeType = "Broke";
            changeTypeFmt = (ITextComponent) new TextComponentString(MessageFormat.format("{0}{1} Block ", this.red, changeType));
        } else {
            if (!changeTypeRaw.trim().toLowerCase().equals("place")) {
                ChatHandler.log.error(String.format("%s%s: Unrecognized block change type. Type: '%s'.", this.getLogPrefixERROR(), selfName, changeTypeRaw));
                return true;
            }
            changeType = "Placed";
            changeTypeFmt = (ITextComponent) new TextComponentString(MessageFormat.format("{0}{1} Block", this.gold, changeType));
        }
        final String blockNameRaw = this.getBlockNameFromId(blockId);
        String blockName;
        if (blockNameRaw == null) {
            blockName = MessageFormat.format("ID-{0}", Integer.valueOf(blockId));
        } else {
            blockName = blockNameRaw;
        }
        final String coords = MessageFormat.format("{0,number,#} {1,number,#} {2,number,#}", Double.valueOf(coordX), Double.valueOf(coordY), Double.valueOf(coordZ));
        final String waypointName = MessageFormat.format("{0}_{1}_{2}", playerName, changeType, blockName);
        final int playerDimensionId = Minecraft.getMinecraft().world.provider.getDimension();
        final ITextComponent jmWaypointFmt = this.getClickableJmWaypointFmt(coordX, Double.valueOf(coordY), coordZ, waypointName, Integer.valueOf(playerDimensionId));
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixJA).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}{1}    ", this.darkAqua, this.mcChatRightPad(playerName, this.PLAYER_NAME_PADDING, "  ")))).appendSibling(changeTypeFmt).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("    {0}{1}    {2}[{3}]  ", this.white, blockName, this.gray, coords))).appendSibling(jmWaypointFmt).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0} ", this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private String getBlockNameFromId(final int id) {
        switch (id) {
            case 1: {
                return "Stone";
            }
            case 2: {
                return "Grass";
            }
            case 3: {
                return "Dirt";
            }
            case 4: {
                return "Cobblestone";
            }
            case 12: {
                return "Sand";
            }
            case 13: {
                return "Gravel";
            }
            case 25: {
                return "Note Block";
            }
            case 26: {
                return "Bed";
            }
            case 47: {
                return "Bookshelf";
            }
            case 49: {
                return "Obsidian";
            }
            case 50: {
                return "Torch";
            }
            case 54: {
                return "Chest";
            }
            case 58: {
                return "Crafting Table";
            }
            case 59: {
                return "Wheat";
            }
            case 61: {
                return "Furnace";
            }
            case 71: {
                return "Iron Door";
            }
            case 85: {
                return "Jukebox";
            }
            case 96: {
                return "Wooden Trapdoor";
            }
            case 98: {
                return "Stone Brick";
            }
            case 116: {
                return "Enchantment Table";
            }
            case 142: {
                return "Potato";
            }
            case 145: {
                return "Anvil";
            }
            case 146: {
                return "Trapped Chest";
            }
            case 154: {
                return "Hopper";
            }
            case 167: {
                return "Iron Trapdoor";
            }
            case 323: {
                return "Sign";
            }
            case 326: {
                return "Water Bucket";
            }
            case 327: {
                return "Lava Bucket";
            }
            case 328: {
                return "Minecart";
            }
            case 330: {
                return "Iron Door";
            }
            case 333: {
                return "Boat";
            }
            default: {
                return null;
            }
        }
    }

    private boolean onJamuteNoIgnores(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*\\*\\s*No Group Ignores\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixJA).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}No muted snitch groups. To (un)mute snitches from a group type {1}/jamute <group>{2}.{3} ", this.gray, this.white, this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onJamuteList(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*Ignore List:\\s*(.+?)\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.matches()) {
            return false;
        }
        if (match.group(1) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final String groupNamesRaw = match.group(1);
        ITextComponent groupNamesFmt = (ITextComponent) new TextComponentString("");
        for (String groupName : groupNamesRaw.split(", ")) {
            groupName = groupName.trim();
            final ClickEvent runCommandJamute = new ClickEvent(ClickEvent.Action.RUN_COMMAND, MessageFormat.format("/jamute {0}", groupName));
            final ITextComponent runCommandJamuteFmt = new TextComponentString(MessageFormat.format("{0}[{1}]", this.gold, groupName)).setStyle(new Style().setClickEvent(runCommandJamute));
            groupNamesFmt = groupNamesFmt.appendSibling((ITextComponent) new TextComponentString("  ")).appendSibling(runCommandJamuteFmt);
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixJA).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Muted snitch groups:", this.gray))).appendSibling(groupNamesFmt).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0} ", this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onJamuteAddFirst(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*Added group \"(.+?)\" to ignore list!\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.matches()) {
            return false;
        }
        if (match.group(1) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final String groupName = match.group(1);
        final ClickEvent runCommandJamute = new ClickEvent(ClickEvent.Action.RUN_COMMAND, MessageFormat.format("/jamute {0}", groupName));
        final ITextComponent runCommandJamuteFmt = new TextComponentString(MessageFormat.format("{0}[{1}]", this.gold, groupName)).setStyle(new Style().setClickEvent(runCommandJamute));
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixJA).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Muted snitches from  ", this.gray))).appendSibling(runCommandJamuteFmt).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0} ", this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onJamuteAddSecond(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*Use /jamute on this group again to unmute\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        event.setCanceled(true);
        return true;
    }

    private boolean onJamuteRemove(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*Removed group \"(.+?)\" from ignore list!\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.matches()) {
            return false;
        }
        if (match.group(1) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final String groupName = match.group(1);
        final ClickEvent runCommandJamute = new ClickEvent(ClickEvent.Action.RUN_COMMAND, MessageFormat.format("/jamute {0}", groupName));
        final ITextComponent runCommandJamuteFmt = new TextComponentString(MessageFormat.format("{0}[{1}]", this.gold, groupName)).setStyle(new Style().setClickEvent(runCommandJamute));
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixJA).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Unmuted snitches from  ", this.gray))).appendSibling(runCommandJamuteFmt).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0} ", this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onNonOwnedDestruction(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("^\\s*Snitch at \\[([-\\w]+?) (-?[0-9]+) (-?[0-9]+) (-?[0-9]+)\\] is owned by (.+?) and is on group: (.+?)\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.matches()) {
            return false;
        }
        if (match.group(1) == null || match.group(2) == null || match.group(3) == null || match.group(4) == null || match.group(5) == null || match.group(6) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final String dimensionNameRaw = match.group(1);
        final double coordX = Double.parseDouble(match.group(2));
        final double coordY = Double.parseDouble(match.group(3));
        final double coordZ = Double.parseDouble(match.group(4));
        final String playerName = match.group(5);
        final String groupName = match.group(6);
        final String coords = MessageFormat.format("{0,number,#} {1,number,#} {2,number,#}", Double.valueOf(coordX), Double.valueOf(coordY), Double.valueOf(coordZ));
        final int playerDimensionId = Minecraft.getMinecraft().world.provider.getDimension();
        String playerDimensionRawName = null;
        switch (playerDimensionId) {
            case 0: {
                playerDimensionRawName = "world";
                break;
            }
            case -1: {
                playerDimensionRawName = "world_nether";
                break;
            }
            case 1: {
                playerDimensionRawName = "world_the_end";
                break;
            }
            default: {
                ChatHandler.log.error(String.format("%s%s: Unrecognized player dimension ID. ID: '%d'.", this.getLogPrefixERROR(), selfName, Integer.valueOf(playerDimensionId)));
                return true;
            }
        }
        final ClickEvent suggestMsg = new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, MessageFormat.format("The snitch at [{0} {1}] was on [{2}], which is owned by {3}.", playerDimensionRawName, coords, groupName, playerName));
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixJA).appendSibling(new TextComponentString(MessageFormat.format("{0}The snitch at [{1}] was reinforced to {2}[{3}]{4}, which is owned by {5}{6}{7}.{8} ", this.gray, coords, this.gold, groupName, this.gray, this.darkAqua, playerName, this.gray, this.reset)).setStyle(new Style().setClickEvent(suggestMsg)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private ITextComponent getFormattedPlayerRank(String playerRank) {
        playerRank = playerRank.trim().toLowerCase();
        ITextComponent playerRankFmt;
        if (playerRank.equals("member") || playerRank.equals("members")) {
            playerRankFmt = (ITextComponent) new TextComponentString(MessageFormat.format("{0}Member", this.white));
        } else if (playerRank.equals("mod") || playerRank.equals("mods")) {
            playerRankFmt = (ITextComponent) new TextComponentString(MessageFormat.format("{0}Mod", this.gold));
        } else if (playerRank.equals("admin") || playerRank.equals("admins")) {
            playerRankFmt = (ITextComponent) new TextComponentString(MessageFormat.format("{0}Admin", this.green));
        } else if (playerRank.equals("owner") || playerRank.equals("owners")) {
            playerRankFmt = (ITextComponent) new TextComponentString(MessageFormat.format("{0}Owner", this.darkAqua));
        } else {
            playerRankFmt = null;
        }
        return playerRankFmt;
    }

    private ITextComponent getFormattedPlayerRankArticles(String playerRank) {
        playerRank = playerRank.trim().toLowerCase();
        ITextComponent playerRankFmt;
        if (playerRank.equals("member") || playerRank.equals("members")) {
            playerRankFmt = (ITextComponent) new TextComponentString(MessageFormat.format("{0}a {1}Member", this.gray, this.white));
        } else if (playerRank.equals("mod") || playerRank.equals("mods")) {
            playerRankFmt = (ITextComponent) new TextComponentString(MessageFormat.format("{0}a {1}Mod", this.gray, this.gold));
        } else if (playerRank.equals("admin") || playerRank.equals("admins")) {
            playerRankFmt = (ITextComponent) new TextComponentString(MessageFormat.format("{0}an {1}Admin", this.gray, this.green));
        } else if (playerRank.equals("owner") || playerRank.equals("owners")) {
            playerRankFmt = (ITextComponent) new TextComponentString(MessageFormat.format("{0}an {1}Owner", this.gray, this.darkAqua));
        } else {
            playerRankFmt = null;
        }
        return playerRankFmt;
    }

    private ITextComponent getFormattedPlayerRankAligned(String playerRank) {
        playerRank = playerRank.trim().toLowerCase();
        ITextComponent playerRankFmt;
        if (playerRank.equals("member") || playerRank.equals("members")) {
            playerRankFmt = (ITextComponent) new TextComponentString(MessageFormat.format("{0}Member", this.white));
        } else if (playerRank.equals("mod") || playerRank.equals("mods")) {
            playerRankFmt = (ITextComponent) new TextComponentString(MessageFormat.format("{0}Mod      ", this.gold));
        } else if (playerRank.equals("admin") || playerRank.equals("admins")) {
            playerRankFmt = (ITextComponent) new TextComponentString(MessageFormat.format("{0}Admin   ", this.green));
        } else if (playerRank.equals("owner") || playerRank.equals("owners")) {
            playerRankFmt = (ITextComponent) new TextComponentString(MessageFormat.format("{0}Owner  ", this.darkAqua));
        } else {
            playerRankFmt = null;
        }
        return playerRankFmt;
    }

    private ITextComponent getFormattedPlayerRankPlural(String playerRank) {
        playerRank = playerRank.trim().toLowerCase();
        ITextComponent playerRankFmt;
        if (playerRank.equals("member") || playerRank.equals("members")) {
            playerRankFmt = (ITextComponent) new TextComponentString(MessageFormat.format("{0}Members", this.white));
        } else if (playerRank.equals("mod") || playerRank.equals("mods")) {
            playerRankFmt = (ITextComponent) new TextComponentString(MessageFormat.format("{0}Mods", this.gold));
        } else if (playerRank.equals("admin") || playerRank.equals("admins")) {
            playerRankFmt = (ITextComponent) new TextComponentString(MessageFormat.format("{0}Admins", this.green));
        } else if (playerRank.equals("owner") || playerRank.equals("owners")) {
            playerRankFmt = (ITextComponent) new TextComponentString(MessageFormat.format("{0}Owners", this.darkAqua));
        } else {
            playerRankFmt = null;
        }
        return playerRankFmt;
    }

    private boolean onCommandNoPermissionError(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*You don't have permission to run that command\\.?\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixNL).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Error:  {1}No permission to run that command.{2} ", this.red, this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onGroupModifyNoPermissionError(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*You do not have permissions to modify this group\\.?\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixNL).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Error:  {1}No permission to modify that group.{2} ", this.red, this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onGroupDoesntExistError(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*The group \"(.+?)\" does not exist\\.?\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        if (match.group(1) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final String groupName = match.group(1);
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixNL).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Error:  {1}The group {2}[{3}] {4}doesn''t exist.{5} ", this.red, this.gray, this.gold, groupName, this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onPlayerHasNeverPlayedBeforeError(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*The player has never played before\\.?\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixNL).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Error:  {1}That player has never joined the server.{2} ", this.red, this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onNlcgGroupExistsError(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*That group is already taken\\.? Try another unique group name\\.?\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixNL).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Error:  {1}Group already exists.{2} ", this.red, this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onNlcgGroupNameTooLongError(final ClientChatReceivedEvent event, final String message, final String messageClean) {
        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*The group name is not allowed to contain more than ([0-9]+?) characters\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        if (match.group(1) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final int maxGroupNameLen = Integer.parseInt(match.group(1));
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixNL).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Error:  {1}Group name length cannot exceeed {2}{3} {4}characters.{5} ", this.red, this.gray, this.white, Integer.valueOf(maxGroupNameLen), this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onNlcgTooManyGroupsWarning(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*You have reached the group limit with ([0-9]+?) groups! Please delete un-needed groups if you wish to create more\\.?\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        if (match.group(1) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final int maxGroupCount = Integer.parseInt(match.group(1));
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixNL).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Warning:  {1}Cannot own more than {2}{3} {4}groups.{5} ", this.gold, this.gray, this.white, Integer.valueOf(maxGroupCount), this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onNlcgTooManyGroupsError(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*You cannot create any more groups! Please delete an un-needed group before making more\\.?\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixNL).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Error:  {1}Cannot own any more groups.{2} ", this.red, this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onNlcgScheduled(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*Group creation request is in process\\.?\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixNL).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Creating the group...{1} ", this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onNlcgSuccess(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*The group (.+?) was successfully created\\.?\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        if (match.group(1) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final String groupName = match.group(1);
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixNL).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Created {1}[{2}]{3} ", this.gray, this.gold, groupName, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onNldgConfirmationFirst(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*To confirm deletion of group: (.+?)\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        if (match.group(1) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final String groupName = match.group(1);
        final ClickEvent action = new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/nldg CONFIRM");
        final ITextComponent actionFmt = new TextComponentString(MessageFormat.format("{0}/nldg CONFIRM ", this.white)).setStyle(new Style().setClickEvent(action));
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixNL).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Confirm deletion of {1}[{2}] {3}by typing ", this.gray, this.gold, groupName, this.gray))).appendSibling(actionFmt).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}within 15s.{1} ", this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onNldgConfirmationSecond(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*use /nldg CONFIRM within ([0-9]+?) seconds\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        if (match.group(1) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final int timeout = Integer.parseInt(match.group(1));
        if (timeout != 15) {
            ChatHandler.log.error(String.format("%s%s: /nldg CONFIRM timeout changed, constact awoo.", this.getLogPrefixERROR(), selfName));
        }
        event.setCanceled(true);
        return true;
    }

    private boolean onNldgConfirmationTimeoutError(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*You did not do /nldg CONFIRM fast enough, you will need to start over\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixNL).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Error:  {1}/nldg CONFIRM {2}timed out.{3} ", this.red, this.white, this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onNldgGuiSuccess(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*(.+?) was successfully deleted\\.?\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        if (match.group(1) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final String groupName = match.group(1);
        if (groupName.trim().toLowerCase().equals("group")) {
            return true;
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixNL).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Deleted {1}[{2}]{3} ", this.gray, this.gold, groupName, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onNldgSuccess(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*Group was successfully deleted\\.?\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixNL).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Deleted the group.{1} ", this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onNllmFirst(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*Members are as follows:\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixNL).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Group members:{1} ", this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onNllmSecond(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*(\\S+?) \\((MEMBERS?|MODS?|ADMINS?|OWNERS?)\\)\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        if (match.group(1) == null || match.group(2) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final String playerName = match.group(1);
        final String playerRank = match.group(2).trim().toLowerCase();
        final ITextComponent playerRankFmt = this.getFormattedPlayerRankAligned(playerRank);
        if (playerRankFmt == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixNL).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}-  ", this.gray))).appendSibling(playerRankFmt.appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("  {0}{1}{2} ", this.darkAqua, playerName, this.reset))));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onNllgPagination(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*Page (\\d+) of (\\d+)\\.?\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        if (match.group(1) == null || match.group(2) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final int currentPage = Integer.parseInt(match.group(1));
        final int totalPages = Integer.parseInt(match.group(2));
        final ITextComponent msg = this.getPagination(currentPage, Integer.valueOf(totalPages), "/nllg");
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private ITextComponent getPagination(final int currentPage, final Integer totalPages, final String command) {


        final String selfName = "ChatFilter";
        ITextComponent navPrevWhitespaceFmt = (ITextComponent) new TextComponentString(StringUtils.repeat("  ", command.length()));
        ITextComponent runCmdPrevFmt = (ITextComponent) new TextComponentString("");
        ITextComponent navNextWhitespaceFmt = (ITextComponent) new TextComponentString("");
        ITextComponent runCmdNextFmt = (ITextComponent) new TextComponentString("");
        if (totalPages == null || totalPages != 1) {
            if (currentPage != 1) {
                navPrevWhitespaceFmt = (ITextComponent) new TextComponentString("  ");
                final ClickEvent runCmdPrev = new ClickEvent(ClickEvent.Action.RUN_COMMAND, MessageFormat.format("{0} {1}", command, Integer.valueOf(currentPage - 1)));
                runCmdPrevFmt = new TextComponentString(MessageFormat.format("{0}{1}", this.gray, command)).setStyle(new Style().setClickEvent(runCmdPrev));
            }
            if (totalPages == null || currentPage != totalPages) {
                navNextWhitespaceFmt = (ITextComponent) new TextComponentString("  ");
                final ClickEvent runCmdNext = new ClickEvent(ClickEvent.Action.RUN_COMMAND, MessageFormat.format("{0} {1}", command, Integer.valueOf(currentPage + 1)));
                runCmdNextFmt = new TextComponentString(MessageFormat.format("{0}{1}", this.gray, command)).setStyle(new Style().setClickEvent(runCmdNext));
            }
        }
        ITextComponent totalPagesFmt;
        if (totalPages != null) {
            totalPagesFmt = (ITextComponent) new TextComponentString(MessageFormat.format("{0}{1}", this.white, totalPages));
        } else {
            totalPagesFmt = (ITextComponent) new TextComponentString(MessageFormat.format("{0}?", this.gray));
        }
        final ITextComponent msg = new TextComponentString(" ").appendSibling(runCmdPrevFmt).appendSibling(navPrevWhitespaceFmt).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}<  {1}{2}{3}/", this.gray, this.white, Integer.valueOf(currentPage), this.gray))).appendSibling(totalPagesFmt).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("  {0}>", this.gray))).appendSibling(navNextWhitespaceFmt).appendSibling(runCmdNextFmt);
        return msg;
    }

    private boolean onNllg(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*(.+?) : \\((MEMBERS?|MODS?|ADMINS?|OWNERS?)\\)\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        if (match.group(1) == null || match.group(2) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final String groupName = match.group(1);
        final String playerRank = match.group(2).trim().toLowerCase();
        final ITextComponent playerRankFmt = this.getFormattedPlayerRankAligned(playerRank);
        if (playerRankFmt == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final ClickEvent suggestCommandNllm = new ClickEvent(ClickEvent.Action.RUN_COMMAND, MessageFormat.format("/nllm {0}", groupName));
        final ITextComponent suggestCommandNllmFmt = new TextComponentString(MessageFormat.format("{0}/nllm", this.gray)).setStyle(new Style().setClickEvent(suggestCommandNllm));
        final ITextComponent suggestCommandNllmWhitespaceFmt = (ITextComponent) new TextComponentString("  ");
        final ClickEvent suggestCommandNlleg = new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, MessageFormat.format("/nlleg {0}", groupName));
        final ITextComponent suggestCommandNllegFmt = new TextComponentString(MessageFormat.format("{0}/nlleg", this.gray)).setStyle(new Style().setClickEvent(suggestCommandNlleg));
        final ITextComponent suggestCommandNllegWhitespaceFmt = (ITextComponent) new TextComponentString("  ");
        ITextComponent suggestCommandNldgFmt;
        ITextComponent suggestCommandNldgWhitespaceFmt;
        if (playerRank.equals("owner") || playerRank.equals("owners")) {
            final ClickEvent suggestCommandNldg = new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, MessageFormat.format("/nldg {0}", groupName));
            suggestCommandNldgFmt = new TextComponentString(MessageFormat.format("{0}/nldg", this.gray)).setStyle(new Style().setClickEvent(suggestCommandNldg));
            suggestCommandNldgWhitespaceFmt = (ITextComponent) new TextComponentString("  ");
        } else {
            suggestCommandNldgFmt = (ITextComponent) new TextComponentString("");
            suggestCommandNldgWhitespaceFmt = (ITextComponent) new TextComponentString("");
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixNL).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}-  ", this.gray))).appendSibling(playerRankFmt.appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("  {0}[{1}]", this.gold, groupName)))).appendSibling(suggestCommandNllmWhitespaceFmt).appendSibling(suggestCommandNllmFmt).appendSibling(suggestCommandNllegWhitespaceFmt).appendSibling(suggestCommandNllegFmt).appendSibling(suggestCommandNldgWhitespaceFmt).appendSibling(suggestCommandNldgFmt).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0} ", this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onNlipPlayerAlreadyAMemberError(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*Player is already a member\\.? ?Use /promoteplayer to change their PlayerType\\.?\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixNL).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Error:  {1}That player is already in the group. To remove them, type {2}/nlrm <group> <player>{3}. To change their rank, type {4}/nlpp <group> <player> <rank>{5}.{6} ", this.red, this.gray, this.white, this.gray, this.white, this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onNlipFirst(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*The invitation has been sent\\.?\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        event.setCanceled(true);
        return true;
    }

    private boolean onNlipSecond(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*Use \\/revoke to Revoke an invite\\.?\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        final ClickEvent action = new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/nlri ");
        final ITextComponent actionFmt = new TextComponentString(MessageFormat.format("{0}/nlri <group> <player>", this.white)).setStyle(new Style().setClickEvent(action));
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixNL).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Invitation sent. To revoke it, type ", this.gray))).appendSibling(actionFmt).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}.{1} ", this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onNlipReceive(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*You have been invited to the group (.+?) by (\\S+?)\\.?\\s*Click this message to accept\\.? If you wish to toggle invites so they always are accepted please run \\/autoaccept\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        if (match.group(1) == null || match.group(2) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final String groupName = match.group(1);
        final String playerName = match.group(2);
        final ClickEvent action = new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, MessageFormat.format("/nlag {0}", groupName));
        final ITextComponent actionFmt = new TextComponentString(MessageFormat.format("{0}/nlag {1}", this.white, groupName)).setStyle(new Style().setClickEvent(action));
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixNL).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Got invited to {1}[{2}] {3}by {4}{5}{6}. To accept, type ", this.gray, this.gold, groupName, this.gray, this.darkAqua, playerName, this.gray))).appendSibling(actionFmt.appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}.{1} ", this.gray, this.reset))));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onNlipReceiveOffline(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*You have been invited to the following groups while you were away\\.? You can accept each invitation by using the command: \\/nlag \\[groupname\\]\\.?\\s*(.+?)\\.?\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        if (match.group(1) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final String groupNamesRaw = match.group(1);
        ITextComponent groupNamesFmt = (ITextComponent) new TextComponentString("");
        for (String groupName : this.deduplicate(groupNamesRaw.split(", "))) {
            groupName = groupName.trim();
            final ClickEvent action = new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, MessageFormat.format("/nlag {0}", groupName));
            groupNamesFmt = groupNamesFmt.appendSibling((ITextComponent) new TextComponentString("  ")).appendSibling(new TextComponentString(MessageFormat.format("{0}[{1}]{2}", this.gold, groupName, this.reset)).setStyle(new Style().setClickEvent(action)));
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixNL).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Got invited to:", this.gray))).appendSibling(groupNamesFmt.appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}. To accept, type {1}/nlag <group name>{2}.{3} ", this.gray, this.white, this.gray, this.reset))));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private String[] deduplicate(final String[] array) {
        final List<String> tmp = Arrays.<String>asList(array);
        final List<String> dedupped = new ArrayList<String>(new LinkedHashSet<String>(tmp));
        String[] arr = new String[dedupped.size()];
        arr = dedupped.<String>toArray(arr);
        return arr;
    }

    private boolean onNlriAlreadyAcceptedError(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*(\\S+?) is already part of that group, use /remove to remove them\\.?\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        if (match.group(1) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final String playerName = match.group(1);
        final ClickEvent action = new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/nlrm ");
        final ITextComponent actionFmt = new TextComponentString(MessageFormat.format("{0}/nlrm <group> {1}", this.white, playerName)).setStyle(new Style().setClickEvent(action));
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixNL).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Error:  {1}{2} {3}is already in the group. To remove them, type ", this.red, this.darkAqua, playerName, this.gray))).appendSibling(actionFmt).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}.{1} ", this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onNlagNotInvitedError(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*You were not invited to that group\\.?\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixNL).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Error:  {1}Not invited to that group.{2} ", this.red, this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onNlagSuccess(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*You have successfully been added to the group as a (MEMBERS?|MODS?|ADMINS?|OWNERS?)\\.?\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        if (match.group(1) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final String playerRank = match.group(1).trim().toLowerCase();
        final ITextComponent playerRankFmt = this.getFormattedPlayerRankArticles(playerRank);
        if (playerRankFmt == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixNL).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Joined the group as ", this.gray))).appendSibling(playerRankFmt).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}.{1} ", this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onNlrmPlayerNotInGroupError(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*That player is not on the group\\.?\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixNL).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Error:  {1}That player is not in the group.{2} ", this.red, this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onNlrmSuccess(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*(\\S+?) has been removed from the group\\.?\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        if (match.group(1) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final String playerName = match.group(1);
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixNL).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Removed {1}{2} {3}from the group.{4} ", this.gray, this.darkAqua, playerName, this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onNlpp(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*(\\S+?) has been added as \\(PlayerType\\) (MEMBERS?|MODS?|ADMINS?|OWNERS?) in \\(Group\\) (.+?)\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        if (match.group(1) == null || match.group(2) == null || match.group(3) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final String playerName = match.group(1);
        final String playerRank = match.group(2).trim().toLowerCase();
        final String groupName = match.group(3);
        final ITextComponent playerRankFmt = this.getFormattedPlayerRank(playerRank);
        if (playerRankFmt == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixNL).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Changed rank of {1}{2} {3}to ", this.gray, this.darkAqua, playerName, this.gray))).appendSibling(playerRankFmt).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format(" {0}in {1}[{2}]{3} ", this.gray, this.gold, groupName, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onNlppReceive(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*You have been promoted to (?:\\(PlayerType\\) )?(members?|mods?|admins?|owners?) in (?:\\(Group\\) )?(.+?)\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        if (match.group(1) == null || match.group(2) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final String playerRank = match.group(1).trim().toLowerCase();
        final String groupName = match.group(2);
        final ITextComponent playerRankFmt = this.getFormattedPlayerRank(playerRank);
        if (playerRankFmt == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixNL).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Rank in {1}[{2}] {3}got changed to ", this.gray, this.gold, groupName, this.gray))).appendSibling(playerRankFmt).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}.{1} ", this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onNllp(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*The permission types are: (.+?)\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        if (match.group(1) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final String permissionNamesRaw = match.group(1);
        ITextComponent permissionNamesFmt = (ITextComponent) new TextComponentString("");
        for (String permissionName : permissionNamesRaw.split(" ")) {
            permissionName = permissionName.trim();
            permissionNamesFmt = permissionNamesFmt.appendSibling((ITextComponent) new TextComponentString("  ")).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}{1}{2}", this.white, permissionName, this.reset)));
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixNL).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Permissions:", this.gray))).appendSibling(permissionNamesFmt.appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0} ", this.reset))));
        Minecraft.getMinecraft().player.sendMessage(msg);
        msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixNL).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}To change permissions of a rank in a group type {1}/nlmp <group> <add/remove> <rank> <permission>{2}.{3} ", this.gray, this.white, this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onNlmpGroupHasPermError(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*This PlayerType already has the PermissionType: (.+?)\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        if (match.group(1) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final String permissionName = match.group(1);
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixNL).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Error:  {1}That player type already has the {2}{3} {4}permission.{5} ", this.red, this.gray, this.white, permissionName, this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onNlmpGroupDoesntHavePermError(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*This PlayerType does not have the PermissionType: (.+?)\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        if (match.group(1) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final String permissionName = match.group(1);
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixNL).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Error:  {1}That player type doesn''t have the {2}{3} {4}permission.{5} ", this.red, this.gray, this.white, permissionName, this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onNlmp(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*The PermissionType: (.+?) was successfully (added|removed) (?:from|to) the PlayerType: (MEMBERS?|MODS?|ADMINS?|OWNERS?)\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        if (match.group(1) == null || match.group(2) == null || match.group(3) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final String permissionName = match.group(1);
        final String actionNameRaw = match.group(2).trim().toLowerCase();
        final String playerRank = match.group(3).trim().toLowerCase();
        ITextComponent actionNameFmt;
        if (actionNameRaw.equals("added")) {
            actionNameFmt = (ITextComponent) new TextComponentString(MessageFormat.format("{0}added {1}to ", this.green, this.gray));
        } else {
            if (!actionNameRaw.equals("removed")) {
                ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
                return true;
            }
            actionNameFmt = (ITextComponent) new TextComponentString(MessageFormat.format("{0}removed {1}from ", this.red, this.gray));
        }
        final ITextComponent playerRankFmt = this.getFormattedPlayerRankPlural(playerRank);
        if (playerRankFmt == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixNL).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}The {1}{2} {3}permission has been ", this.gray, this.white, permissionName, this.gray))).appendSibling(actionNameFmt).appendSibling(playerRankFmt).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}.{1} ", this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onNlleg(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*You have been removed from the group\\.?\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixNL).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Left the group.{1} ", this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onIeFirst(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*\\(([0-9]+?)/([0-9]+?)\\) exchanges present\\.?\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        if (match.group(1) == null || match.group(2) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final int itemExchangeExchangesPresentCount = Integer.parseInt(match.group(1));
        final int itemExchangeExchangesTotalCount = Integer.parseInt(match.group(2));
        TextFormatting itemExchangeExchangesTotalCountColor;
        if (itemExchangeExchangesPresentCount == 1 && itemExchangeExchangesTotalCount > 1) {
            itemExchangeExchangesTotalCountColor = this.white;
        } else {
            itemExchangeExchangesTotalCountColor = this.gray;
        }
        final ITextComponent msg = (ITextComponent) new TextComponentString("");
        this.itemExchangeExchangePrefix = msg.appendSibling(this.prefixIE).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}[{1}/{2}{3}{4}]  ", this.gray, Integer.valueOf(itemExchangeExchangesPresentCount), itemExchangeExchangesTotalCountColor, Integer.valueOf(itemExchangeExchangesTotalCount), this.gray)));
        event.setCanceled(true);
        return true;
    }

    private boolean onIeSecond(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        if (this.itemExchangeExchangePrefix == null) {
            return false;
        }
        Pattern regex = Pattern.compile("(?i)^\\s*\\u00A7r\\u00A7eInput:");
        Matcher match = regex.matcher(message);
        if (!match.find()) {
            return false;
        }
        regex = Pattern.compile("(?i)^\\s*Input: ([0-9]+) ?(.+?)\\s*$");
        match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        if (match.group(1) == null || match.group(2) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final int exchangeInputItemCount = Integer.parseInt(match.group(1));
        final String exchangeInputItem = match.group(2);
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixIE).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}----------------{1} ", this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.itemExchangeExchangePrefix).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Input:     {1} {2}x {3}{4}{5} ", this.white, Integer.valueOf(exchangeInputItemCount), this.gray, this.white, exchangeInputItem, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onIeThird(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        if (this.itemExchangeExchangePrefix == null) {
            return false;
        }
        Pattern regex = Pattern.compile("(?i)^\\s*\\u00A7r\\u00A7eOutput:");
        Matcher match = regex.matcher(message);
        if (!match.find()) {
            return false;
        }
        regex = Pattern.compile("(?i)^\\s*Output: ([0-9]+) ?(.+?)\\s*$");
        match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        if (match.group(1) == null || match.group(2) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final int exchangeOutputItemCount = Integer.parseInt(match.group(1));
        final String exchangeOutputItem = match.group(2);
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.itemExchangeExchangePrefix).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Output:   {1} {2}x {3}{4}{5} ", this.white, Integer.valueOf(exchangeOutputItemCount), this.gray, this.white, exchangeOutputItem, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onIeFourth(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        if (this.itemExchangeExchangePrefix == null) {
            return false;
        }
        final Pattern regex = Pattern.compile("(?i)^\\s*([0-9]+?) exchanges? available\\.?\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        if (match.group(1) == null) {
            ChatHandler.log.error(String.format("%s%s: Chat message changed. Message: '%s'.", this.getLogPrefixERROR(), selfName, messageClean));
            return true;
        }
        final int exchangesCount = Integer.parseInt(match.group(1));
        TextFormatting exchangesCountColor;
        if (exchangesCount == 0) {
            exchangesCountColor = this.red;
        } else {
            exchangesCountColor = this.white;
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.itemExchangeExchangePrefix).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Trades:  {1}{2}{3} ", this.white, exchangesCountColor, Integer.valueOf(exchangesCount), this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onIeExchangeNoItemsLeftError(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*Chest does not have enough of the output\\.?\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixIE).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Error:  {1}Exchange failed (trade depleted).{2} ", this.red, this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onIeExchangeSuccess(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*Successful exchange!\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixIE).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Exchange succeeded.{1} ", this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    private boolean onIeExchangeCreationSuccess(final ClientChatReceivedEvent event, final String message, final String messageClean) {

        final String selfName = "ChatFilter";
        final Pattern regex = Pattern.compile("(?i)^\\s*Created exchange successfully\\.?\\s*$");
        final Matcher match = regex.matcher(messageClean);
        if (!match.find()) {
            return false;
        }
        ITextComponent msg = (ITextComponent) new TextComponentString("");
        msg = msg.appendSibling(this.prefixIE).appendSibling((ITextComponent) new TextComponentString(MessageFormat.format("{0}Exchange created.{1} ", this.gray, this.reset)));
        Minecraft.getMinecraft().player.sendMessage(msg);
        event.setCanceled(true);
        return true;
    }

    static {
        ChatHandler.log = LogManager.getLogger("chatfilters");
    }
}
