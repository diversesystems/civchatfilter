package com.awoo;

import net.minecraft.util.text.ITextComponent;

public class MessageDistance implements Comparable<MessageDistance> {
    public ITextComponent message;
    public Double distance;
    public double coordX;
    public double coordY;
    public double coordZ;

    public MessageDistance() {
        super();
    }

    @Override
    public int compareTo(final MessageDistance msgd) {
        if (this.distance != null && msgd.distance != null && this.distance > msgd.distance) {
            return 1;
        }
        return -1;
    }

}
