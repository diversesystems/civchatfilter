package com.awoo;

public class Snitch {
    public String name;
    public String previousName;
    public String groupName;
    public String worldName;
    public Double cullTime;
    public Double coordX;
    public Double coordY;
    public Double coordZ;
    public String coords;

    public Snitch() {
        super();
    }
}
