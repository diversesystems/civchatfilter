package com.awoo;

import net.minecraft.client.settings.KeyBinding;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent;

public class KeyHandler {
    public KeyBinding toggle;

    public KeyHandler() {
        super();
        ClientRegistry.registerKeyBinding(this.toggle = new KeyBinding("Toggle", 38, "Chatfilters"));
    }

    @SubscribeEvent
    public void onKeyPress(final InputEvent.KeyInputEvent event) {
        if (this.toggle.isPressed()) {
            if ((boolean) Chatfilters.DISABLED) {
                Chatfilters.DISABLED = Boolean.valueOf(false);
            } else {
                Chatfilters.DISABLED = Boolean.valueOf(true);
            }
        }
    }
}
